// program to compute the amplitudes for a given set of js
// and all possibile intertwiners and store it in a text file
// uses sl2cfoam library

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#include "sl2cfoam.h"
#include "config.h"

// disable warning on dspin -> int conversion
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"

// Bool types.
typedef enum { false, true } bool;

#define check_integer_3sum_continue(tl1, tl2, tl3) \
    { if ((tl1+tl2+tl3) % 2 != 0) {continue;} }

#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define DIM(tj) (tj+1)

#define fail(str) {\
            fprintf(stderr, str);\
            fprintf(stderr, "\n");\
            exit(EXIT_FAILURE);\
        }

int main (int argc, char* argv[]) {

    if (argc < 3) {
        fprintf(stderr, "Usage: %s [-v] [-as] [dspin] [-d] [delta tl] -o [out folder]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    // input spins
    dspin tjs[10];

    // delta l
    dspin delta_tl;
    delta_tl = 0;

    // verbose flags
    bool verbose;
    verbose = false;

    char filepath[512];

    /////////////////////////////////////////////////////////////////////
    // flag parsing
    /////////////////////////////////////////////////////////////////////

    int opt;
    dspin tj;
    while ((opt = getopt(argc, argv, "vbs:a:d:o:")) != -1) {
        switch (opt) {
        case 'v': verbose = true; break;
        case 's': // set spin individually
            sscanf(optarg, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", 
                   &tjs[0], &tjs[1], &tjs[2], &tjs[3], &tjs[4],
                   &tjs[5], &tjs[6], &tjs[7], &tjs[8], &tjs[9]);
            break;
        case 'a': // set all spin equals
            if (!(tj = (dspin)atoi(optarg))) {
                fail("error parsing spin argument");
            };
            int i;
            for (i = 0; i < 10; i++) {
                tjs[i] = tj;   
            }
            break;
        case 'd':
            // crude parsing 
            // check for 0
            if (strcmp(optarg, "0") == 0) {
                delta_tl = 0;
                break;
            }
            // check for delta_tl > 0
            if (!(delta_tl = (dspin)atoi(optarg))) {
                fail("error parsing delta l argument");
            };
            break;
        case 'o':
            strcpy(filepath, optarg);
            strcat(filepath, "/");
            break;
        default:
            fprintf(stderr, "Usage: %s [-v] [-as] [dspin] [-d] [delta tl] -o [out folder]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    // for measuring timings
    struct timespec start, stop;
    double dtime;

    /////////////////////////////////////////////////////////////////////
    // init library and setup spins
    /////////////////////////////////////////////////////////////////////

    float gamma = 1.2f; // Immirzi parameter

    // initialize sl2cfoam library
    struct sl2cfoam_config libconf;
    libconf.data_folder = "../data_sl2cfoam/";
    libconf.store_ampls = (int)true;
    libconf.verbosity = 0;

    #if USE_REDUCIBLE_COUPLING == 1
        libconf.coupling = SL2CFOAM_COUPLING_REDUCIBLE;
    #else
        libconf.coupling = SL2CFOAM_COUPLING_IRREDUCIBLE;
    #endif

    sl2cfoam_init_conf(&libconf);

    // get boundary spins

    dspin tj1, tj2, tj3, tj4, tj5,
          tj6, tj7, tj8, tj9, tj10;

    tj1 = tjs[0];
    tj2 = tjs[1];
    tj3 = tjs[2];
    tj4 = tjs[3];
    tj5 = tjs[4];
    tj6 = tjs[5];
    tj7 = tjs[6];
    tj8 = tjs[7];
    tj9 = tjs[8];
    tj10 = tjs[9];

    /////////////////////////////////////////////////////////////////////
    // open file for writing
    /////////////////////////////////////////////////////////////////////

    // write spins in first line of the file
    // filename format "tj1-tj2...tj10.ampl"
    char filename[50];

    #if USE_REDUCIBLE_COUPLING == 1
        sprintf(filename, "%02d-%02d-%02d-%02d-%02d-%02d-%02d-%02d-%02d-%02d_D%d.redamplext", 
            tj1, tj2, tj3, tj4, tj5, tj6, tj7, tj8, tj9, tj10, delta_tl);
    #else
        sprintf(filename, "%02d-%02d-%02d-%02d-%02d-%02d-%02d-%02d-%02d-%02d_D%d.amplext", 
            tj1, tj2, tj3, tj4, tj5, tj6, tj7, tj8, tj9, tj10, delta_tl);
    #endif

    strcat(filepath, filename);

    // write just one to file
    FILE *file = fopen(filepath, "w");
    if (file == NULL){
        fail("error opening file for writing");
    }

#if USE_REDUCIBLE_COUPLING == 1

    /////////////////////////////////////////////////////////////////////
    // compute boundaries
    /////////////////////////////////////////////////////////////////////

    dspin mini2, maxi2;
    dspin mini3, maxi3;
    dspin mini4, maxi4;
    dspin mini5, maxi5;

    // fourth external intertwiner
    // j6 j5 j10 j9
    mini4 = (dspin) max(abs(tj6-tj5), abs(tj10-tj9));
    maxi4 = (dspin) min(tj6+tj5, tj10+tj9);

    // second external intertwiner
    // j10 j7 j1 j2
    mini2 = (dspin) max(abs(tj10-tj7), abs(tj1-tj2));
    maxi2 = (dspin) min(tj10+tj7, tj1+tj2);

    // third external intertwiner 
    // j9 j8 j3 j1
    mini3 = (dspin) max(abs(tj9-tj8), abs(tj3-tj1));
    maxi3 = (dspin) min(tj9+tj8, tj3+tj1);

    // fifth external intertwiner
    // l4 l6 l7 l8
    mini5 = (dspin) max(abs(tj4-tj6), abs(tj7-tj8));
    maxi5 = (dspin) min(tj4+tj6, tj7+tj8);

    /////////////////////////////////////////////////////////////////////
    // loop over intertwiners for hashing
    /////////////////////////////////////////////////////////////////////

    if (verbose) {
        printf("Start caching... ");
        fflush(stdout);
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    dspin ti4;
    for (ti4 = mini4; ti4 <= maxi4; ti4 += 2) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

    dspin mini11, mini12, maxi11, maxi12, mini1, maxi1;
    dspin ti1, ti2, ti3, ti5;

    for (ti2 = mini2; ti2 <= maxi2; ti2 += 2) {
        check_integer_3sum_continue(tj10, tj7, ti2);
        check_integer_3sum_continue(tj1, tj2, ti2);

    for (ti3 = mini3; ti3 <= maxi3; ti3 += 2) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

    for (ti5 = mini5; ti5 <= maxi5; ti5 += 2) {
        check_integer_3sum_continue(tj4, tj6, ti5);
        check_integer_3sum_continue(tj7, tj8, ti5);

    // first external intertwiner
    // j2 j3 j5 j4
    mini11 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    mini12 = (dspin) max(abs(ti2-ti3), abs(ti5-ti4));
    mini1 = max(mini11, mini12);
    maxi11 = (dspin) min(tj2+tj3, tj5+tj4);
    maxi12 = (dspin) min(ti2+ti3, ti5+ti4);
    maxi1 = min(maxi11, maxi12);

    for (ti1 = mini1; ti1 <= maxi1; ti1 += 2) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

        // hash boosts and 15js
        sl2cfoam_hash_symbols(tj1, tj2, tj3, tj4, tj5,
                              tj6, tj7, tj8, tj9, tj10,
                              ti4, delta_tl, gamma);

    } // ti1
    } // ti5
    } // ti3
    } // ti2
    } // ti4

    if (verbose) {
        clock_gettime(CLOCK_MONOTONIC, &stop);
        dtime = (double)(stop.tv_sec - start.tv_sec);
        dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
        printf("done caching. Time elapsed: %f seconds.\n", dtime);
    }

    /////////////////////////////////////////////////////////////////////
    // PARALLEL loop over intertwiners for amplitude
    /////////////////////////////////////////////////////////////////////

    if (verbose) {
        printf("Start computing amplitudes... ");
        fflush(stdout);
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

	#pragma omp parallel
    {

    // per-thread initialization
    sl2cfoam_init_thread();
        
    #pragma omp for
    for (ti4 = mini4; ti4 <= maxi4; ti4 += 2) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

    dspin mini11, mini12, maxi11, maxi12, mini1, maxi1;
    dspin ti1, ti2, ti3, ti5;

    for (ti2 = mini2; ti2 <= maxi2; ti2 += 2) {
        check_integer_3sum_continue(tj10, tj7, ti2);
        check_integer_3sum_continue(tj1, tj2, ti2);

    for (ti3 = mini3; ti3 <= maxi3; ti3 += 2) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

    for (ti5 = mini5; ti5 <= maxi5; ti5 += 2) {
        check_integer_3sum_continue(tj4, tj6, ti5);
        check_integer_3sum_continue(tj7, tj8, ti5);

    // first external intertwiner
    // j2 j3 j5 j4
    mini11 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    mini12 = (dspin) max(abs(ti2-ti3), abs(ti5-ti4));
    mini1 = max(mini11, mini12);
    maxi11 = (dspin) min(tj2+tj3, tj5+tj4);
    maxi12 = (dspin) min(ti2+ti3, ti5+ti4);
    maxi1 = min(maxi11, maxi12);

    for (ti1 = mini1; ti1 <= maxi1; ti1 += 2) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

        // just compute the value, it will be
        // stored in the cache system
        sl2cfoam_four_ampl(tj1, tj2, tj3, tj4, tj5,
                           tj6, tj7, tj8, tj9, tj10,
                           ti1, ti2, ti3, ti4, ti5,
                           delta_tl, gamma);

    } // ti1
    } // ti5
    } // ti3
    } // ti2
    } // ti4

    sl2cfoam_free_thread();

    } // omp parallel

    if (verbose) {
        clock_gettime(CLOCK_MONOTONIC, &stop);
        dtime = (double)(stop.tv_sec - start.tv_sec);
        dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
        printf("done computing amplitudes. Time elapsed: %f seconds.\n", dtime);
    }

    /////////////////////////////////////////////////////////////////////
    // print loop, amplitudes are already cached
    //////////////////////////////////////////////////////////////////////

    if (verbose) {
        printf("Start writing to file... ");
        fflush(stdout);
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    double ampl;

    dspin mini11, mini12, maxi11, maxi12, mini1, maxi1;
    dspin ti1, ti2, ti3, ti5;

    // second external intertwiner
    for (ti2 = mini2; ti2 <= maxi2; ti2 += 2) {
        check_integer_3sum_continue(tj10, tj7, ti2);
        check_integer_3sum_continue(tj1, tj2, ti2);

    // third external intertwiner 
    for (ti3 = mini3; ti3 <= maxi3; ti3 += 2) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

    // fourth external intertwiner
    for (ti4 = mini4; ti4 <= maxi4; ti4 += 2) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

    // fifth external intertwiner
    for (ti5 = mini5; ti5 <= maxi5; ti5 += 2) {
        check_integer_3sum_continue(tj4, tj6, ti5);
        check_integer_3sum_continue(tj7, tj8, ti5);

    // first external intertwiner
    // j2 j3 j5 j4
    mini11 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    mini12 = (dspin) max(abs(ti2-ti3), abs(ti5-ti4));
    mini1 = max(mini11, mini12);
    maxi11 = (dspin) min(tj2+tj3, tj5+tj4);
    maxi12 = (dspin) min(ti2+ti3, ti5+ti4);
    maxi1 = min(maxi11, maxi12);

    // first external intertwiner
    // j2 j3 j5 j4
    for (ti1 = mini1; ti1 <= maxi1; ti1 += 2) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

        ampl = sl2cfoam_four_simplex(tj1, tj2, tj3, tj4, tj5,
                                     tj6, tj7, tj8, tj9, tj10,
                                     ti1, ti2, ti3, ti4, ti5,
                                     delta_tl, gamma);

        // write just one to file
        fprintf(file, "%02d %02d %02d %02d %02d %.3e\n",
                ti1, ti2, ti3, ti4, ti5, ampl);
        fflush(file);
    
    } // ti1
    } // ti5
    } // ti4
    } // ti3
    } // ti2

    if (verbose) {
        clock_gettime(CLOCK_MONOTONIC, &stop);
        dtime = (double)(stop.tv_sec - start.tv_sec);
        dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
        printf("done writing to file. Time elapsed: %f seconds.\n", dtime);
    }

#else

    /////////////////////////////////////////////////////////////////////
    // compute boundaries
    /////////////////////////////////////////////////////////////////////

    dspin mini1, maxi1;
    dspin mini2, maxi2;
    dspin mini3, maxi3;
    dspin mini4, maxi4;
    dspin mini5, maxi5;

    // first external intertwiner
    // j2 j3 j5 j4
    mini1 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    maxi1 = (dspin) min(tj2+tj3, tj5+tj4);

    // second external intertwiner
    // j1 j10 j7 j2
    mini2 = (dspin) max(abs(tj1-tj10), abs(tj7-tj2));
    maxi2 = (dspin) min(tj1+tj10, tj7+tj2);

    // third external intertwiner 
    // j9 j8 j3 j1
    mini3 = (dspin) max(abs(tj9-tj8), abs(tj3-tj1));
    maxi3 = (dspin) min(tj9+tj8, tj3+tj1);

    // fourth external intertwiner
    // j6 j5 j10 j9
    mini4 = (dspin) max(abs(tj6-tj5), abs(tj10-tj9));
    maxi4 = (dspin) min(tj6+tj5, tj10+tj9);

    // fifth external intertwiner
    // l4 l7 l8 l6
    mini5 = (dspin) max(abs(tj4-tj7), abs(tj8-tj6));
    maxi5 = (dspin) min(tj4+tj7, tj8+tj6);


    /////////////////////////////////////////////////////////////////////
    // hash boosters and 15j symbols
    /////////////////////////////////////////////////////////////////////

    if (verbose) {
        printf("Start caching symbols... ");
        fflush(stdout);
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    dspin ti4;
    for (ti4 = mini4; ti4 <= maxi4; ti4 += 2) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

        // hash boosts and 15js
        sl2cfoam_hash_symbols(tj1, tj2, tj3, tj4, tj5,
                              tj6, tj7, tj8, tj9, tj10,
                              ti4, delta_tl, gamma);

    } // ti4

    if (verbose) {
        clock_gettime(CLOCK_MONOTONIC, &stop);
        dtime = (double)(stop.tv_sec - start.tv_sec);
        dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
        printf("done caching. Time elapsed: %f seconds.\n", dtime);
    }

    /////////////////////////////////////////////////////////////////////
    // PARALLEL loop over intertwiners for amplitude
    /////////////////////////////////////////////////////////////////////

    if (verbose) {
        printf("Start computing amplitudes... ");
        fflush(stdout);
        clock_gettime(CLOCK_MONOTONIC, &start);
    }
    

	#pragma omp parallel
    {

    // per-thread initialization
    sl2cfoam_init_thread();
        
    #pragma omp for
    for (ti4 = mini4; ti4 <= maxi4; ti4 += 2) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

    dspin ti1, ti2, ti3, ti5;

    for (ti5 = mini5; ti5 <= maxi5; ti5 += 2) {
        check_integer_3sum_continue(tj4, tj6, ti5);
        check_integer_3sum_continue(tj7, tj8, ti5);

    for (ti1 = mini1; ti1 <= maxi1; ti1 += 2) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

    for (ti2 = mini2; ti2 <= maxi2; ti2 += 2) {
        check_integer_3sum_continue(tj10, tj7, ti2);
        check_integer_3sum_continue(tj1, tj2, ti2);

    for (ti3 = mini3; ti3 <= maxi3; ti3 += 2) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

        // just compute the value, it will be
        // stored in the cache system
        sl2cfoam_four_ampl(tj1, tj2, tj3, tj4, tj5,
                           tj6, tj7, tj8, tj9, tj10,
                           ti1, ti2, ti3, ti4, ti5,
                           delta_tl, gamma);

    } // ti3
    } // ti2
    } // ti1
    } // ti5
    } // ti4

    sl2cfoam_free_thread();

    } // omp parallel


    if (verbose) {
        clock_gettime(CLOCK_MONOTONIC, &stop);
        dtime = (double)(stop.tv_sec - start.tv_sec);
        dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
        printf("done computing amplitudes. Time elapsed: %f seconds.\n", dtime);
    }

    /////////////////////////////////////////////////////////////////////
    // print loop, amplitudes are already cached
    // respect the for loop order of the computation phase
    //////////////////////////////////////////////////////////////////////

    if (verbose) {
        printf("Start writing to file... ");
        fflush(stdout);
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    // print from sl2cfoam cache
    dspin ti1, ti2, ti3, ti5;
    double ampl;

    // fourth external intertwiner
    for (ti4 = mini4; ti4 <= maxi4; ti4 += 2) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

    // fifth external intertwiner
    for (ti5 = mini5; ti5 <= maxi5; ti5 += 2) {
        check_integer_3sum_continue(tj4, tj6, ti5);
        check_integer_3sum_continue(tj7, tj8, ti5);

    // first external intertwiner
    // j2 j3 j5 j4
    for (ti1 = mini1; ti1 <= maxi1; ti1 += 2) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

    // second external intertwiner
    for (ti2 = mini2; ti2 <= maxi2; ti2 += 2) {
        check_integer_3sum_continue(tj10, tj7, ti2);
        check_integer_3sum_continue(tj1, tj2, ti2);

    // third external intertwiner 
    for (ti3 = mini3; ti3 <= maxi3; ti3 += 2) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

        ampl = sl2cfoam_four_ampl(tj1, tj2, tj3, tj4, tj5,
                                tj6, tj7, tj8, tj9, tj10,
                                ti1, ti2, ti3, ti4, ti5,
                                delta_tl, gamma);

        fprintf(file, "%02d %02d %02d %02d %02d %.3e\n",
                ti1, ti2, ti3, ti4, ti5, ampl);
        fflush(file);

    } // ti3
    } // ti2
    } // ti1
    } // ti5
    } // ti4

    if (verbose) {
        clock_gettime(CLOCK_MONOTONIC, &stop);
        dtime = (double)(stop.tv_sec - start.tv_sec);
        dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
        printf("done writing to file. Time elapsed: %f seconds.\n", dtime);
    }

#endif

    /////////////////////////////////////////////////////////////////////
    // cleanup
    /////////////////////////////////////////////////////////////////////

    fclose(file);
    sl2cfoam_free();

    return 0;

}
