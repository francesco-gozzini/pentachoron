// program to compute the amplitudes for a given set of js
// and all possibile intertwiners and store it in a text file

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "pentachoron.h"
#include "integration.h"
#include "utils.h"
#include "common.h"

#define fail(str) {\
            fprintf(stderr, str);\
            fprintf(stderr, "\n");\
            exit(EXIT_FAILURE);\
        }


int main (int argc, char* argv[]) {

    if (argc < 3) {
        fprintf(stderr, "Usage: %s [-b] [-v/-V] [-as] [dspin] [-d] [delta tl] -o [out folder]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    // input spins
    dspin tjs[10];

    bool all_equal;
    all_equal = false;

    // delta l
    dspin delta_tl;
    delta_tl = 0;

    // verbose flags
    bool verbose, very_verbose;
    verbose = very_verbose = false;

    // BF flag
    bool do_BF;
    do_BF = false;

    char filepath[512];

    /////////////////////////////////////////////////////////////////////
    // flag parsing
    /////////////////////////////////////////////////////////////////////

    int opt;
    dspin tjarg;
    while ((opt = getopt(argc, argv, "vVbs:a:d:o:")) != -1) {
        switch (opt) {
        case 'v': verbose = true; break;
        case 'V': very_verbose = verbose = true; break;
        case 'b': do_BF = true; break;
        case 's': // set spin individually
            sscanf(optarg, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", 
                   &tjs[0], &tjs[1], &tjs[2], &tjs[3], &tjs[4],
                   &tjs[5], &tjs[6], &tjs[7], &tjs[8], &tjs[9]);
            break;
        case 'a': // set all spin equals
            
            if (!(tjarg = (dspin)atoi(optarg))) {
                fail("error parsing spin argument");
            };
            int i;
            for (i = 0; i < 10; i++) {
                tjs[i] = tjarg;   
            }
            all_equal = true;
            break;
        case 'd':
            // crude parsing 
            // check for 0
            if (strcmp(optarg, "0") == 0) {
                delta_tl = 0;
                break;
            }
            // check for delta_tl > 0
            if (!(delta_tl = (dspin)atoi(optarg))) {
                fail("error parsing delta l argument");
            };
            break;
        case 'o':
            strcpy(filepath, optarg);
            strcat(filepath, "/");
            break;
        default:
            fprintf(stderr, "Usage: %s [-v] [-as] [dspin] [-d] [delta tl] -o [out folder]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }
 
    /////////////////////////////////////////////////////////////////////
    // init library and setup spins
    // TODO: study dependence on gamma
    /////////////////////////////////////////////////////////////////////

    struct pntc_setup s;
    s.gamma = 1.2;

    // const size_t totpoints = 100;
    // double* X = (double*)calloc(totpoints, sizeof(double));
    // size_t npoints[3] = {70, 20, 10};

    const size_t totpoints = 200;
    double* X = (double*)calloc(totpoints, sizeof(double));
    size_t npoints[3] = {120, 60, 20};

    // const size_t totpoints = 500;
    // double* X = (double*)calloc(totpoints, sizeof(double));
    // size_t npoints[3] = {300, 150, 50};

    // const size_t totpoints = 1000;
    // double* X = (double*)calloc(totpoints, sizeof(double));
    // size_t npoints[3] = {600, 300, 100};

    if (all_equal) {

        // TODO: check for grid limits
        double tj = (double)tjs[0];
        double xs[4] = {0.01, 3*tj, 6*tj, 9*tj};
        size_t nxs = 4;
        piecewise_grid(X, xs, npoints, nxs);


    } else {

        double xs[4] = {0.01, 15.0, 30.0, 100.0};
        size_t nxs = 4;
        piecewise_grid(X, xs, npoints, nxs);

    }

    struct pntc_integration_params ip;
    ip.gsl_epsrel = 0.01;
    ip.upper_bound = 100.0;
    ip.X = X;
    ip.totpoints = totpoints;
    s.ip = &ip;

    pntc_init(&s);

    dspin tj1, tj2, tj3, tj4, tj5,
          tj6, tj7, tj8, tj9, tj10;

    tj1 = tjs[0];
    tj2 = tjs[1];
    tj3 = tjs[2];
    tj4 = tjs[3];
    tj5 = tjs[4];
    tj6 = tjs[5];
    tj7 = tjs[6];
    tj8 = tjs[7];
    tj9 = tjs[8];
    tj10 = tjs[9];

    struct pntc_amplitude_params p;
    p.delta_tl = delta_tl;
    p.verbose = very_verbose;

    // assign external spins
    struct pntc_boundary_state bs;
    int i;
    for (i = 0; i < 10; i++) {
        bs.tjs[i] = tjs[i];
    }

    // write spins in first line of the file
    // filename format "tj1-tj2...tj10.ampl"
    char filename[40];

    sprintf(filename, "%d-%d-%d-%d-%d-%d-%d-%d-%d-%d_D%d.ampl", 
            tj1, tj2, tj3, tj4, tj5, tj6, tj7, tj8, tj9, tj10, delta_tl);
    strcat(filepath, filename);

    FILE *file = fopen(filepath, "w");
    if (file == NULL){
        fail("error opening file for writing");
    }


    /////////////////////////////////////////////////////////////////////
    // loop over intertwiners
    /////////////////////////////////////////////////////////////////////

    double ampl;
    int istep = 2;

    // for measuring timings
    struct timespec start, stop;
    double dtime;


#if USE_REDUCIBLE_COUPLING == 1

    dspin mini11, mini12, maxi11, maxi12, mini1, maxi1, ti1;
    dspin mini2, maxi2, ti2;
    dspin mini3, maxi3, ti3;
    dspin mini4, maxi4, ti4;
    dspin mini5, maxi5, ti5;

    // second external intertwiner
    // j10 j7 j1 j2
    mini2 = (dspin) max(abs(tj10-tj7), abs(tj1-tj2));
    maxi2 = (dspin) min(tj10+tj7, tj1+tj2);
    for (ti2 = mini2; ti2 <= maxi2; ti2 += istep) {
        check_integer_3sum_continue(tj10, tj7, ti2);
        check_integer_3sum_continue(tj1, tj2, ti2);

        bs.tis[1] = ti2;

    // third external intertwiner 
    // j9 j8 j3 j1
    mini3 = (dspin) max(abs(tj9-tj8), abs(tj3-tj1));
    maxi3 = (dspin) min(tj9+tj8, tj3+tj1);
    for (ti3 = mini3; ti3 <= maxi3; ti3 += istep) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

        bs.tis[2] = ti3;

    // fourth external intertwiner
    // j6 j5 j10 j9
    mini4 = (dspin) max(abs(tj6-tj5), abs(tj10-tj9));
    maxi4 = (dspin) min(tj6+tj5, tj10+tj9);
    for (ti4 = mini4; ti4 <= maxi4; ti4 += istep) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

        bs.tis[3] = ti4;

    // fifth external intertwiner
    // l4 l6 l7 l8
    mini5 = (dspin) max(abs(tj4-tj6), abs(tj7-tj8));
    maxi5 = (dspin) min(tj4+tj6, tj7+tj8);
    for (ti5 = mini5; ti5 <= maxi5; ti5 += istep) {
        check_integer_3sum_continue(tj4, tj6, ti5);
        check_integer_3sum_continue(tj7, tj8, ti5);

        bs.tis[4] = ti5;

    // first external intertwiner
    // j2 j3 j5 j4
    mini11 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    mini12 = (dspin) max(abs(ti2-ti3), abs(ti5-ti4));
    mini1 = max(mini11, mini12);
    maxi11 = (dspin) min(tj2+tj3, tj5+tj4);
    maxi12 = (dspin) min(ti2+ti3, ti5+ti4);
    maxi1 = min(maxi11, maxi12);
    for (ti1 = mini1; ti1 <= maxi1; ti1 += istep) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

        bs.tis[0] = ti1;

        if (verbose) {
            printf("Computing amplitude for intertwiners %d %d %d %d %d ... ",
                    ti1, ti2, ti3, ti4, ti5);
            clock_gettime(CLOCK_MONOTONIC, &start);
        }
        
        if (do_BF) {
            ampl = pntc_amplitude_BF(&p, &bs);
        } else {
            ampl = pntc_amplitude(&p, &bs);
        }
        
        if (verbose) {
            clock_gettime(CLOCK_MONOTONIC, &stop);
            dtime = (double)(stop.tv_sec - start.tv_sec);
            dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
            printf("done in %f seconds. Amplitude = %.20f\n", dtime, ampl);
        }

        fprintf(file, "%02d %02d %02d %02d %02d %.3e\n",
                ti1, ti2, ti3, ti4, ti5, ampl);
        fflush(file);
    
    } // ti1
    } // ti2
    } // ti3
    } // ti4
    } // ti5

#else

    dspin mini1, maxi1, ti1;
    dspin mini2, maxi2, ti2;
    dspin mini3, maxi3, ti3;
    dspin mini4, maxi4, ti4;
    dspin mini5, maxi5, ti5;

    // first external intertwiner
    // j2 j3 j5 j4
    mini1 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    maxi1 = (dspin) min(tj2+tj3, tj5+tj4);
    for (ti1 = mini1; ti1 <= maxi1; ti1 += istep) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

        bs.tis[0] = ti1;


    // second external intertwiner
    // j1 j10 j7 j2
    mini2 = (dspin) max(abs(tj1-tj10), abs(tj7-tj2));
    maxi2 = (dspin) min(tj1+tj10, tj7+tj2);
    for (ti2 = mini2; ti2 <= maxi2; ti2 += istep) {
        check_integer_3sum_continue(tj1, tj10, ti2);
        check_integer_3sum_continue(tj7, tj2, ti2);

        bs.tis[1] = ti2;

    // third external intertwiner 
    // j9 j8 j3 j1
    mini3 = (dspin) max(abs(tj9-tj8), abs(tj3-tj1));
    maxi3 = (dspin) min(tj9+tj8, tj3+tj1);
    for (ti3 = mini3; ti3 <= maxi3; ti3 += istep) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

        bs.tis[2] = ti3;

    // fourth external intertwiner
    // j6 j5 j10 j9
    mini4 = (dspin) max(abs(tj6-tj5), abs(tj10-tj9));
    maxi4 = (dspin) min(tj6+tj5, tj10+tj9);
    for (ti4 = mini4; ti4 <= maxi4; ti4 += istep) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

        bs.tis[3] = ti4;

    // fifth external intertwiner
    // l4 l7 l8 l6
    mini5 = (dspin) max(abs(tj4-tj7), abs(tj8-tj6));
    maxi5 = (dspin) min(tj4+tj7, tj8+tj6);
    for (ti5 = mini5; ti5 <= maxi5; ti5 += istep) {
        check_integer_3sum_continue(tj4, tj7, ti5);
        check_integer_3sum_continue(tj8, tj6, ti5);

        bs.tis[4] = ti5;

        if (verbose) {
            printf("Computing amplitude for intertwiners %d %d %d %d %d ... ",
                    ti1, ti2, ti3, ti4, ti5);
            clock_gettime(CLOCK_MONOTONIC, &start);
        }
        

        if (do_BF) {
            ampl = pntc_amplitude_BF(&p, &bs);
        } else {
            ampl = pntc_amplitude(&p, &bs);
        }

        if (verbose) {
            clock_gettime(CLOCK_MONOTONIC, &stop);
            dtime = (double)(stop.tv_sec - start.tv_sec);
            dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;
            printf("done in %f seconds. Amplitude = %.20f\n", dtime, ampl);
        }
        

        // write just one to file
        fprintf(file, "%02d %02d %02d %02d %02d %.3e\n",
                ti1, ti2, ti3, ti4, ti5, ampl);
        fflush(file);


    } // ti1
    } // ti2
    } // ti3
    } // ti4
    } // ti5

#endif

    /////////////////////////////////////////////////////////////////////
    // cleanup
    /////////////////////////////////////////////////////////////////////

    fclose(file);
    pntc_free();
    free(X);

    return 0;

}
