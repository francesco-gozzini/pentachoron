#ifndef __PENTACHORON_H__
#define __PENTACHORON_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#include "common.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Library for computing the EPRL vertex amplitudes.
////////////////////////////////////////////////////////////////////////

// Parameters for integration routines.
struct pntc_integration_params {
	double gsl_epsrel;  // relative error in GSL integration
	double upper_bound; // large upper bound for (in-)finite integration
	double* X;          // mesh for simple integration
	size_t totpoints;   // number of points on the mesh
};

// Parameters for computation of amplitudes.
// TODO: adjust verbose output/logging
struct pntc_amplitude_params {
	dspin delta_tl; // 2 * delta_j parameters in the amplitude sums
	bool verbose;   // if enable verbose output
};

// Contains general parameters for setup of the library.
struct pntc_setup {
	double gamma;                       // the Barbero-Immirzi constant
	struct pntc_integration_params* ip; // parameters for integrations
};

// Boundary state of pentachoron represented by a list of
// 10 double-spins and 5 double-intertwiners.
struct pntc_boundary_state {
	dspin tis[5];
	dspin tjs[10];
};

// Always call this function before using the library.
void pntc_init(struct pntc_setup* s);

// Always call this function when finished  using the library.
void pntc_free();

// Computes the EPRL vertex amplitude for the given boundary state.
double pntc_amplitude(struct pntc_amplitude_params* ampl_p, struct pntc_boundary_state* b);

// Computes the vertex amplitude for the given boundary state for the BF theory
// (pure 15j symbol).
double pntc_amplitude_BF(struct pntc_amplitude_params* ampl_p, struct pntc_boundary_state* b);
			
/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_H__*/