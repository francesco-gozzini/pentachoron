% Computes the expectation value of the Casimir
% operator squared
function E = ExpValObsSquare(data)
    
    E = zeros(1,length(data));
    nsq = Norm(data).^2;
    for i=1:5
        for j=1:length(data)
            v = cell2mat(data(j));
            ampl = v(:,6);
            snf = 1 ./ ((v(:,1)+1).*(v(:,2)+1).*(v(:,3)+1).*(v(:,4)+1).*(v(:,5)+1));
            k = (v(:,i)./2);
            E(i,j) = sum((k.*(k + 1)).^2.*ampl.^2 .* snf)/nsq(j);
        end
    end

end