% Takes as input a dspin 2j and a matrix loaded from an amplitudes file 
% ((2j+1)^5 x 6 matrix) and returns a matrix (2j+1) x (2j+1)^4 with amplitudes
% as entries that is ready for SVD decomposition.
function M = Schmidt(tj, A) 

    M = zeros(tj+1, (tj+1).^4);
    
    [h, ~] = size(A);
    for rind=1:h
        r = A(rind,:);
        
        % first dspin gives row index
        i = 1 + floor(r(1) / 2);
        
        % other dspins give column (flattening)
        j = flatten(tj, r(2:5));
        
        snf = 1 / ((r(1)+1)*(r(2)+1)*(r(3)+1)*(r(4)+1)*(r(5)+1));
        M(i,j) = r(6) / sqrt(snf); % normalized state
        
    end

end

function j = flatten(tj, ds)
    j = 1 + floor(ds(4) / 2) + (tj+1)*floor(ds(3) / 2) ...
                             + (tj+1)*(tj+1)*floor(ds(2) / 2) ...
                             + (tj+1)*(tj+1)*(tj+1)*floor(ds(1) / 2);
end
