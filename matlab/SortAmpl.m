% Sortes an amplitude file by decreasing amplitude
function s = SortAmpl(b)

    babs = abs(b);
    [~,idx] = sort(babs(:,6), 'descend');
    s = babs(idx,:);
    
end