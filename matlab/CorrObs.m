% Computes the correlation function between intertwiner
% with index k and others. C(k,j) will contain the 
% autocorrelation == variance of iw. k.
function C = CorrObs(k, data)
    
    C = zeros(5, length(data));
    Cstd = zeros(5, length(data));
    nsq = Norm(data).^2;
    E = ExpValObs(data);
    for i=1:5
        for j=1:length(data)
            v = cell2mat(data(j));
            snf = 1 ./ ((v(:,1)+1).*(v(:,2)+1).*(v(:,3)+1).*(v(:,4)+1).*(v(:,5)+1));
            kk = (v(:,k)./2);
            ki = (v(:,i)./2);
            C(i,j) = sum((kk.*(kk + 1)) .* (ki.*(ki + 1)) .* v(:,6).^2 .* snf)/nsq(j);
        end
    end
    
    % std dev of iw. k
    for i=1:5
        for j=1:length(data)
            v = cell2mat(data(j));
            snf = 1 ./ ((v(:,1)+1).*(v(:,2)+1).*(v(:,3)+1).*(v(:,4)+1).*(v(:,5)+1));
            ki = (v(:,i)./2);
            expsq = sum((ki.*(ki + 1)).^2 .* v(:,6).^2 .* snf)/nsq(j);
            Cstd(i,j) = sqrt(expsq -  E(i,j)*E(i,j));
        end
    end
    
    % normalize other correlations
    for i=1:5
        
        for j=1:length(data)
            C(i,j) = (C(i,j) - E(k,j)*E(i,j)) / (Cstd(i,j)*Cstd(k,j)); % normalized
            % what if variance == 0?
            % C(i,j) = (C(i,j) - E(k,j)*E(i,j));
        end
        
    end

end