% Computes the normalization factor sqrt(< W | W >)
function norm = Norm(data)
    
    norm = zeros(1,length(data));
    for i=1:length(data)
        v = cell2mat(data(i));
        ampl = v(:,6); 
        snf = 1 ./ ((v(:,1)+1).*(v(:,2)+1).*(v(:,3)+1).*(v(:,4)+1).*(v(:,5)+1)); % spin network squared norm
        norm(i) = sqrt(sum(ampl.^2 .* snf));
    end

end