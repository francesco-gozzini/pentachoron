% Computes the probability density of the 2 dim space of 2 varying
% intertwiners. k1, k2 are the indices of the intertwiners.
function [J1, J2, P] = P2D(k1, k2, data, j)

    v = cell2mat(data(j));
    [h, ~] = size(v);
    
    J1 = [];
    J2 = [];
    for i=1:h
        
        r = v(i,:);
        if ~ismember(r(k1),J1)
            J1 = [J1 r(k1)];
        end
        
        if ~ismember(r(k2),J2)
            J2 = [J2 r(k2)];
        end
        
    end
    
    P = zeros(length(J1), length(J2));
    for i=1:h
        
        r = v(i,:);
        j1_ind = find(J1 == r(k1));
        j2_ind = find(J2 == r(k2));
        P(j1_ind, j2_ind) = P(j1_ind, j2_ind) + (r(6)^2 / ((r(1)+1)*(r(2)+1)*(r(3)+1)*(r(4)+1)*(r(5)+1)));
        
    end
    
    J1 = J1 ./ 2;
    J2 = J2 ./ 2;
    P = P ./ sum(sum(P)); % normalize
    
end