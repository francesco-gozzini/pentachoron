% Removes sl2cfoam final normalization on amplitude
function a = Denormalize(b)

    a = b;
    a(:, 6) = a(:, 6) ./ ((a(:, 1)+1).*(a(:, 2)+1).*(a(:, 3)+1).*(a(:, 4)+1).*(a(:, 5)+1));

end