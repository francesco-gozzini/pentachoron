% import spin files into a cell array
% append to data
% version for Lorentzian four simplex
function newdata = ImportAmplLorentzian(data, folder, lambdas, tds, ext)

    if length(lambdas) ~= length(tds)
       fprintf("error, different size for lambdasElo and deltas");
       return
    end
    
    newdata = data;

    for i=1:length(lambdas)
        
        lambda = lambdas(i);
        tjs = [4 4 4 4 10 10 4 4 10 10] .* lambda;
        td = tds(i);
        file = sprintf("%d-%d-%d-%d-%d-%d-%d-%d-%d-%d_D%d", ...
                       tjs(1), tjs(2), tjs(3), tjs(4), tjs(5), tjs(6), tjs(7), tjs(8), tjs(9), tjs(10), td);
        file = file + "." + ext;
        file
        fprintf("importing lambda = %d, tdl = %d file... ", lambda, td);
        newdata{length(data) + i} = importdata(folder + file, ' ');
        fprintf("done\n"); 
        
    end

end