% Computes the average volume of one of 
% the boundary nodes
function vol = ExpValVol(k, Q, data, j)

    A = cell2mat(data(j));
    [h, ~] = size(A);
    
    % scan for different intertwiner spins
    ks = [];
    for i=1:h
        r = A(i,:);
        if ~ismember(r(k), ks)
            ks = [ks r(k)];
        end
    end
    
    ks = sort(ks, 'ascend');
    
    vol = 0;
    
    for i1 = 1:length(ks)
        
        k1 = ks(i1);
        idx = find(A(:,k) == k1);
        Ak1 = A(idx, :);
        
        for i2 = 1:length(ks);
            
            k2 = ks(i2);
            idx = find(A(:,k) == k2);
            Ak2 = A(idx, :);

            q = Q(i1, i2);
            prod = ((Ak1(:,6)')*Ak2(:,6));
            d = q * prod;
            
            vol = vol + d;
        
        end  
    end
    
    vol = sqrt(2)/3 * sqrt(abs(vol));
        
end