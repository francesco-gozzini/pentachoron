% import spin files into a cell array
% append to data
function newdata = ImportAmpl(data, folder, dspins, tds, ext)

    if length(dspins) ~= length(tds)
       fprintf("error, different size for spins and deltas");
       return
    end
    
    newdata = data;

    for i=1:length(dspins)
        
        tj = dspins(i);
        td = tds(i);
        file = sprintf("%d-%d-%d-%d-%d-%d-%d-%d-%d-%d_D%d", tj, tj, tj, tj, tj, tj, tj, tj, tj, tj, td);
        file = file + "." + ext;
        fprintf("importing dspin = %d, tdl = %d file... ", tj, td);
        newdata{length(data) + i} = importdata(folder + file, ' ');
        fprintf("done\n"); 
        
    end

end