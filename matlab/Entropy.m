% Computes the entropy of any intertwiner wrt the rest of the simplex
% as function of increasing spins.
function S = Entropy(tjs, data)
    
    S = zeros(1,length(data));
    
    for i=1:length(data)
        
            A = cell2mat(data(i));
            tj = tjs(i);
            
            s = svd(Schmidt(tj, A));
            p = s.^2 ./ sum(s.^2);
            
            S(i) = -sum(p .* log2(p));
            
    end

end