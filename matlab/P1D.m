% Computes the probability density of the 1 dim space of a varying
% intertwiner. k is the index of the intertwiner.
function [J, P] = P1D(k, data, j)

    v = cell2mat(data(j));
    [h, ~] = size(v);
    
    J = [];
    P = [];
    c = [];
    for i=1:h
        
        r = v(i,:);
        if ~ismember(r(k),J)
            J = [J r(k)];
            P = [P 0];
            c = [c 0];
        end
        
        jind = find(J == r(k));
        snf = 1 / ((r(1)+1)*(r(2)+1)*(r(3)+1)*(r(4)+1)*(r(5)+1));

        y = (r(6)^2 * snf) - c(jind);
        t = P(jind) + y;
        c(jind) = (t - P(jind)) - y;
 
        P(jind) = t; 
        
    end
    
    J = J ./ 2;
    P = P ./ sum(P); % normalize
    
end