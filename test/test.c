#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <mpfr.h>
#include <mpc.h>

#include "common.h"
#include "njm.h"
#include "boosts.h"
#include "pentachoron.h"
#include "plot.h"
#include "integration.h"
#include "cgamma.h"
#include "utils.h"

int main() {

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    struct pntc_setup s;
    s.gamma = 1.2;

    // const size_t totpoints = 600;
    // double* X = (double*)calloc(totpoints, sizeof(double));
    // double xs[5] = {0.01, 10.0, 20.0, 50, 100.0};
    // size_t npoints[4] = {250, 200, 100, 50};
    // size_t nxs = 5;
    // piecewise_grid(X, xs, npoints, nxs);

    const size_t totpoints = 120;
    double* X = (double*)calloc(totpoints, sizeof(double));
    double xs[4] = {0.01, 10.0, 20.0, 100.0};
    size_t npoints[3] = {60, 40, 20};
    size_t nxs = 4;
    piecewise_grid(X, xs, npoints, nxs);


    struct pntc_integration_params ip;
    ip.gsl_epsrel = 0.05;
    ip.upper_bound = 200.0;
    ip.X = X;
    ip.totpoints = totpoints;
    s.ip = &ip;

    pntc_init(&s);

    /////////////////////////////////
    // test gamma
    /////////////////////////////////

// #if USE_ARBITRARY_PRECISION == 1

//     mpc_t x;
//     mpc_init2(x, PRECISION);

//     mpc_set_si(x, 21, MPC_RNDNN);
//     complex_gamma(x, x);
//     printf("20! = %s\n", mpc_get_str(10, 0, x, MPC_RNDNN));

//     mpc_clear(x);

// #endif

    /////////////////////////////////
    // test 15j
    /////////////////////////////////

    // // symmetries
    // double r1, r2;
    // r1 = wigner_15j(1, 1, 1, 1, 1,
    //                 2, 2, 2, 2, 2,
    //                 3, 3, 3, 3, 3);
    
    // r2 = wigner_15j(3, 3, 3, 3, 3,
    //                 2, 2, 2, 2, 2,
    //                 1, 1, 1, 1, 1);

    // printf("%.20f  ?==  %.20f\n", r1, r2);

    // r1 = wigner_15j(1, 1, 1, 1, 3,
    //                 2, 2, 2, 2, 2,
    //                 3, 3, 3, 3, 1);
    
    // r2 = wigner_15j(1, 1, 1, 3, 3,
    //                 2, 2, 2, 2, 2,
    //                 3, 3, 3, 1, 1);

    // printf("%.20f  ?==  %.20f\n", r1, r2);

    // dspin tj;
    // double wig15j;
    // const int JMAX = 150;
    // const int J0 = 50;
    // const int step = 2;
    // const int N = (JMAX-J0)/step;
    // double XJ[N];
    // double YJ[N];
    // int i;
    // clock_t dt15j;

    // // irreducible
    // START_TIMING(dt15j);
    // i = 0;
    // for (tj = 2*J0; tj <= 2*JMAX; tj += 2*step) {
    //     wig15j = wigner_15j(tj, tj, tj, tj, tj,
    //                         tj, tj, tj, tj, tj,
    //                         tj, tj, tj, tj, tj);
    //     XJ[i] = tj / 2;
    //     YJ[i] = wig15j;
    //     i++;
    // }
    // STOP_TIMING(dt15j);

    // printf("15j irreducible done in %f seconds.\n", GET_TIMING(dt15j));
    // print_points(XJ, YJ, (size_t) N);

    // //exit(0);

    // // reducible
    // START_TIMING(dt15j);
    // i = 0;
    // for (tj = 2*J0; tj <= 2*JMAX; tj += 2*step) {
    //     wig15j = wigner_15j_reducible(tj, tj, tj, tj, tj,
    //                         tj, tj, tj, tj, tj,
    //                         tj, tj, tj, tj, tj);
    //     YJ[i] = wig15j;
    //     i++;
    // }
    // STOP_TIMING(dt15j);

    // printf("15j reducible done in %f seconds.\n", GET_TIMING(dt15j));
    // print_points(XJ, YJ, (size_t) N);

    // exit(0);

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    // dspin params[10] = {2, 2, 2, 2, 5, 3, 5, 3, 2, 2};
    // dspin params[10] = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    // dspin params[10] = {20, 20, 20, 20, 20, 20, 24, 24, 20, 20};
    // dspin params[10] = {2, 2, 2, 2, 2, 4, 2, 6, 4, 4};
    // dspin params[10] = {12, 12, 12, 12, 12, 18, 16, 16, 22, 8};
    // dspin params[10] = {12, 12, 12, 12, 12, 12, 12, 12, 10, 14};
    // dspin params[10] = {1, 1, 1, 1, 3, 1, 5, 5, 0, 2};
    // dspin params[10] = {20, 20, 20, 20, 20, 20, 24, 24, 20, 20};
    // dspin params[10] = {40, 40, 40, 40, 40, 40, 40, 40, 4, 0};
    // dspin params[10] = {60, 60, 60, 60, 60, 60, 60, 60, 4, 4};
    // dspin params[10] = {24, 26, 24, 26, 30, 32, 30, 32, 2, 2};
    // dspin params[10] = {26, 26, 26, 26, 26, 26, 26, 26, 14, 10};

    // dspin params[10] = {1,1,1,1,1,1,1,1,2,2};

    // gsl_function F;
    // F.function = &B4_integrand;

    // dspin params[10] = {12, 12, 12, 12, 12, 18, 16, 16, 22, 8};
    // F.params = params;
    // plot(&F, X, totpoints);


    // dspin params2[10] = {26, 26, 26, 26, 26, 26, 26, 26, 14, 10};
    // F.params = params2;
    // plot(&F, X, totpoints);

    //printf("B4(...) = %.15f", B4(60, 60, 60, 60, 60, 60, 60, 60, 4, 4));
    //printf("B4(...) = %.15f\n", B4(8, 2, 12, 4, 8, 2, 12, 4, 8, 8));
    //printf("B4(...) = %.15f\n", B4(8, 10, 10, 10, 8, 10, 10, 10, 2, 4));
    //printf("B4(...) = %.15f\n", B4(6, 6, 6, 6, 8, 6, 10, 10, 4, 2));

    printf("B4(...) = %.15f", B4(30, 30, 30, 30, 30, 30, 30, 30, 4, 4));

    // B4(6, 6, 6, 6, 8, 6, 8, 8, 4, 2)
    // cambio j2 -> sempre soppressa
    // cambio j4 -> NON soppresso
    // cambio l4 -> NON soppresso
    // B4(6, 6, 6, 4, 8, 6, 10, 10, 4, 2)) NON SOPPRESSO
    // B4(6, 6, 6, 6, 8, 6, 10, 10, 4, 2)) SOPPRESSO
    // B4(6, 6, 4, 4, 8, 6, 10, 10, 4, 2) SOPPRESSO
    // B4(6, 6, 4, 6, 6, 8, 10, 10, 4, 2) NON SOPPRESSO
    // B4(8, 8, 4, 6, 8, 8, 8, 10, 4, 2) SOPPRESSO
    // B4(6, 6, 6, 6, 8, 6, 6, 8, 4, 2) NON SOPPRESSO

    // printf("B4(...) = %.15f\n", B4(26, 26, 26, 26, 26, 26, 26, 26, 14, 10));

    // double res;
    // res = B4(2, 2, 2, 2, 2, 2, 2, 6, 0, 4);
    // printf("B4(1, 1, 1, 1, 1, 1, 1, 3; %d, %d) == %g\n", 0, 2, res);

    // res = B4(2, 2, 2, 2, 2, 2, 2, 6, 4, 4);
    // printf("B4(1, 1, 1, 1, 1, 1, 1, 3; %d, %d) == %g\n", 4, 4, res);

    // exit(0);

    //////////////////////////////////////////////////////////////
    // test B3 and B4
    ///////////////////////////////////////////////////////////////


    // int j;
    // double res;

    // res = B3(2, 2, 2, 2, 2, 2);
    // printf("B3(2, 2, 2, 2, 2, 2) == %g\n", res);
    // res = B3(2, 2, 2, 2, 2, 4);
    // printf("B3(2, 2, 2, 2, 2, 4) == %g\n", res);
    // res = B3(2, 2, 2, 2, 4, 4);
    // printf("B3(2, 2, 2, 2, 4, 4) == %g\n", res);
    // res = B3(2, 2, 2, 2, 4, 6);
    // printf("B3(2, 2, 2, 2, 4, 6) == %g\n", res);
    // res = B3(2, 2, 2, 2, 6, 6);
    // printf("B3(2, 2, 2, 2, 6, 6) == %g\n", res);
    // res = B3(2, 2, 2, 4, 4, 4);
    // printf("B3(2, 2, 2, 4, 4, 4) == %g\n", res);
    // res = B3(2, 2, 2, 4, 4, 6);
    // printf("B3(2, 2, 2, 4, 4, 6) == %g\n", res);
    // res = B3(2, 2, 2, 4, 6, 6);
    // printf("B3(2, 2, 2, 4, 6, 6) == %g\n", res);
    // res = B3(2, 2, 2, 6, 6, 6);
    // printf("B3(2, 2, 2, 6, 6, 6) == %g\n", res);
    // res = B3(2, 2, 4, 2, 2, 4);
    // printf("B3(2, 2, 2, 4, 4, 4) == %g\n", res);
    // res = B3(2, 2, 4, 2, 4, 4);
    // printf("B3(2, 2, 4, 2, 4, 4) == %g\n", res);
    // res = B3(2, 2, 4, 2, 4, 6);
    // printf("B3(2, 2, 4, 2, 4, 6) == %g\n", res);
    // res = B3(2, 2, 4, 2, 6, 6);
    // printf("B3(2, 2, 4, 2, 6, 6) == %g\n", res);

    // res = B3(2, 4, 6, 2, 4, 6);
    // printf("B3(2, 4, 6, 2, 4, 6) == %g\n", res);
    // res = B3(2, 4, 6, 2, 6, 6);
    // printf("B3(2, 4, 6, 2, 6, 6) == %g\n", res);

    // double res;

    // int j1, j2, j3, l1, l2, l3;
    // for (j1 = 1; j1 <= 3; j1++) {
    //     for (j2 = j1; j2 <= 3; j2++) {
    //         for (j3 = j2; j3 <= 3; j3++) {
    //             for (l1 = max(1, j1); l1 <= 3; l1++) {
    //                 for (l2 = max(l1, j2); l2 <= 3; l2++) {
    //                     for (l3 = max(l2, j3); l3 <= 3; l3++) {

    //                         if (!triangle(2*j1, 2*j2, 2*j3)) {
    //                             continue;
    //                         }

    //                         if (!triangle(2*l1, 2*l2, 2*l3)) {
    //                             continue;
    //                         }

    //                         res = B3(2*j1, 2*j2, 2*j3, 2*l1, 2*l2, 2*l3);
    //                         printf("B3(%d, %d, %d, %d, %d, %d) == %.8f\n",
    //                                j1, j2, j3, l1, l2, l3, res);

    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    //     dspin tj;
    // int j;
    // for (j = 1; j <= 8; j++) {

    //     tj = (dspin)(2*j);
    //     res = B4(tj, tj, tj, tj, tj, tj, tj, tj, 0, 0);
    //     printf("B4(j == %d; 0, 0) == %.8f\n", j, res);
    //     res = B4(tj, tj, tj, tj, tj, tj, tj, tj, tj - 2, tj + 2);
    //     printf("B4(j == %d; j-1, j+1) == %.8f\n", j, res);
    //     res = B4(tj, tj, tj, tj, tj, tj, tj, tj, tj, tj);
    //     printf("B4(j == %d; j, j) == %.8f\n", j, res);
    //     res = B4(tj, tj, tj, tj, tj, tj, tj, tj, tj, tj + 4);
    //     printf("B4(j == %d; j, j+2) == %.8f\n", j, res);

    // }

    

    // printf("B4 ASYMTPTOTICS - SCALING SPIN\n\n");

    // const int SPINS = 24;
    // double XJ[SPINS];
    // double YJ[SPINS];

    // dspin tj;
    // double res;
    // int i = 0;
    // for (tj = 1; tj <= SPINS; tj++) {
    //     res = B4(tj, tj, tj, tj, tj, tj, tj, tj, 0, 0);
    //     printf("Done B4 nr %d...\n", i+1);
    //     XJ[i] = (double)tj/2.0;
    //     YJ[i] = res;
    //     i++;
    // }

    // print_points(XJ, YJ, SPINS);


    // printf("B4 ASYMTPTOTICS - SCALING DELTA SPIN\n\n");

    // const int DSPINS = 30;
    // double XJ[DSPINS];
    // double YJ[DSPINS];
    // clock_t dtb4;

    // dspin tj = 2;
    // dspin dtj;
    // double res;
    // int i = 0;
    // for (dtj = 28; dtj <= 2*DSPINS + 2; dtj += 2) {
    //     START_TIMING(dtb4);
    //     res = B4(tj, tj, tj, tj, tj + dtj, tj + dtj, tj + dtj, tj + dtj, 0, 0);
    //     STOP_TIMING(dtb4);
    //     printf("Done B4 nr %d in %f seconds. dtj = %d, res = %g...\n", i+1, GET_TIMING(dtb4), dtj, res);
    //     XJ[i] = (double)dtj/2.0;
    //     YJ[i] = res;
    //     i++;
    // }

    // print_points(XJ, YJ, DSPINS);

    pntc_free();
    free(X);

}