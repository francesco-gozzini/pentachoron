#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "common.h"
#include "htbl.h"
#include "utils.h"

int main() {

    const dspin TJ = 4;
    const dspin tdl = 0;

    dspin tj1 = TJ;
    dspin tj2 = TJ;
    dspin tj3 = TJ;
    dspin tj4 = TJ;
    dspin tj5 = TJ;
    dspin tj6 = TJ;
    dspin tj7 = TJ;
    dspin tj8 = TJ;
    dspin tj9 = TJ;
    dspin tj10 = TJ;

    dspin mini1, maxi1, ti1;
    dspin mini2, maxi2, ti2;
    dspin mini3, maxi3, ti3;
    dspin mini4, maxi4, ti4;
    dspin mini5, maxi5, ti5;

    dspin istep = 2;
    dspin lstep = 2;
    dspin kstep = 2;

    // first external intertwiner
    // j2 j3 j5 j4
    mini1 = (dspin) max(abs(tj2-tj3), abs(tj5-tj4));
    maxi1 = (dspin) min(tj2+tj3, tj5+tj4);
    for (ti1 = mini1; ti1 <= maxi1; ti1 += istep) { 
        check_integer_3sum_continue(tj2, tj3, ti1);
        check_integer_3sum_continue(tj5, tj4, ti1);

    // second external intertwiner
    // j1 j10 j7 j2
    mini2 = (dspin) max(abs(tj1-tj10), abs(tj7-tj2));
    maxi2 = (dspin) min(tj1+tj10, tj7+tj2);
    for (ti2 = mini2; ti2 <= maxi2; ti2 += istep) {
        check_integer_3sum_continue(tj1, tj10, ti2);
        check_integer_3sum_continue(tj7, tj2, ti2);


    // third external intertwiner 
    // j9 j8 j3 j1
    mini3 = (dspin) max(abs(tj9-tj8), abs(tj3-tj1));
    maxi3 = (dspin) min(tj9+tj8, tj3+tj1);
    for (ti3 = mini3; ti3 <= maxi3; ti3 += istep) {
        check_integer_3sum_continue(tj9, tj8, ti3);
        check_integer_3sum_continue(tj3, tj1, ti3);

    // fourth external intertwiner
    // j6 j5 j10 j9
    mini4 = (dspin) max(abs(tj6-tj5), abs(tj10-tj9));
    maxi4 = (dspin) min(tj6+tj5, tj10+tj9);
    for (ti4 = mini4; ti4 <= maxi4; ti4 += istep) {
        check_integer_3sum_continue(tj6, tj5, ti4);
        check_integer_3sum_continue(tj10, tj9, ti4);

    // fifth external intertwiner
    // l4 l7 l8 l6
    mini5 = (dspin) max(abs(tj4-tj7), abs(tj8-tj6));
    maxi5 = (dspin) min(tj4+tj7, tj8+tj6);
    for (ti5 = mini5; ti5 <= maxi5; ti5 += istep) {
        check_integer_3sum_continue(tj4, tj7, ti5);
        check_integer_3sum_continue(tj8, tj6, ti5);


    dspin tl1, tl2, tl3, tl4, tl5,
          tl6, tl7, tl8, tl9, tl10;

    // 4 ls are fixed on the boundary
    tl5 = tj5;
    tl6 = tj6;
    tl9 = tj9;
    tl10 = tj10;

    dspin mink1, maxk1, tk1;
    dspin mink2, maxk2, tk2;
    dspin mink3, maxk3, tk3;
    dspin mink5, maxk5, tk5;


    for (tl1 = tj1; tl1 <= tj1 + tdl; tl1 += lstep) {
    for (tl2 = tj2; tl2 <= tj2 + tdl; tl2 += lstep) {
    for (tl3 = tj3; tl3 <= tj3 + tdl; tl3 += lstep) {
    for (tl4 = tj4; tl4 <= tj4 + tdl; tl4 += lstep) {
    for (tl7 = tj7; tl7 <= tj7 + tdl; tl7 += lstep) {
    for (tl8 = tj8; tl8 <= tj8 + tdl; tl8 += lstep) {


        // first virtual intertwiner
        // l2 l3 l5 l4
        mink1 = (dspin) max(abs(tl2-tl3), abs(tl5-tl4));
        maxk1 = (dspin) min(tl2+tl3, tl5+tl4);
        for (tk1 = mink1; tk1 <= maxk1; tk1 += kstep) {
            check_integer_3sum_continue(tl2, tl3, tk1);
            check_integer_3sum_continue(tl5, tl4, tk1);

            uint32_t hash1 = b4_hash(tj2, tj3, tj5, tj4, tl2, tl3, tl5, tl4, ti1, tk1);
            printf("loop k1: (%d %d %d %d - %d %d %d %d - %d %d) -> %u \n", 
                   tj2, tj3, tj5, tj4, tl2, tl3, tl5, tl4, ti1, tk1, hash1);


        // second virtual intertwiner
        // l1 l10 l7 l2
        mink2 = (dspin) max(abs(tl1-tl10), abs(tl7-tl2));
        maxk2 = (dspin) min(tl1+tl10, tl7+tl2);
        for (tk2 = mink2; tk2 <= maxk2; tk2 += kstep) {
            check_integer_3sum_continue(tl1, tl10, tk2);
            check_integer_3sum_continue(tl7, tl2, tk2);

            uint32_t hash2 = b4_hash(tj1, tj10, tj7, tj2, tl1, tl10, tl7, tl2, ti2, tk2);
            printf("  loop k2: (%d %d %d %d - %d %d %d %d - %d %d) -> %u \n", 
                   tj1, tj10, tj7, tj2, tl1, tl10, tl7, tl2, ti2, tk2, hash2);


        // third virtual intertwiner 
        // l9 l8 l3 l1
        mink3 = (dspin) max(abs(tl9-tl8), abs(tl3-tl1));
        maxk3 = (dspin) min(tl9+tl8, tl3+tl1);
        for (tk3 = mink3; tk3 <= maxk3; tk3 += kstep) {
            check_integer_3sum_continue(tl9, tl8, tk3);
            check_integer_3sum_continue(tl3, tl1, tk3);

            uint32_t hash3 = b4_hash(tj9, tj8, tj3, tj1, tl9, tl8, tl3, tl1, ti3, tk3);
            printf("    loop k3: (%d %d %d %d - %d %d %d %d - %d %d) -> %u \n", 
                   tj9, tj8, tj3, tj1, tl9, tl8, tl3, tl1, ti3, tk3, hash3);

        // fifth virtual intertwiner
        // l4 l7 l8 l6
        mink5 = (dspin) max(abs(tl4-tl7), abs(tl8-tl6));
        maxk5 = (dspin) min(tl4+tl7, tl8+tl6);
        for (tk5 = mink5; tk5 <= maxk5; tk5 += kstep) {
            check_integer_3sum_continue(tl4, tl7, tk5);
            check_integer_3sum_continue(tl8, tl6, tk5);

            uint32_t hash5 = b4_hash(tj4, tj7, tj8, tj6, tl4, tl7, tl8, tl6, ti5, tk5);
            printf("      loop k5: (%d %d %d %d - %d %d %d %d - %d %d) -> %u \n", 
                   tj4, tj7, tj8, tj6, tl4, tl7, tl8, tl6, ti5, tk5, hash5);


        } // tk1
        } // tk2
        } // tk3
        } // tk5
        
    } // tl1
    } // tl2
    } // tl3
    } // tl4
    } // tl7
    } // tl8

    } // ti1
    } // ti2
    } // ti3
    } // ti4
    } // ti5

}