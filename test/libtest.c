#include <stdio.h>
#include <stdlib.h>

#include "integration.h"
#include "pentachoron.h"

int main() {

    printf("Testing library...\n");

    struct pntc_setup s;
    s.gamma = 1.2;

    // init my integration grid

    const size_t totpoints = 100;
    double* X = (double*)calloc(totpoints, sizeof(double));
    size_t npoints[2] = {80, 20};

    // const size_t totpoints = 300;
    // double* X = (double*)calloc(totpoints, sizeof(double));
    // size_t npoints[2] = {220, 80};

    // const size_t totpoints = 1000;
    // double* X = (double*)calloc(totpoints, sizeof(double));
    // size_t npoints[2] = {800, 200};

    double xs[3] = {1e-6, 20.0, 50.0};
    size_t nxs = 3;
    piecewise_grid(X, xs, npoints, nxs);

    struct pntc_integration_params ip;
    ip.gsl_epsrel = 0.01;
    ip.upper_bound = 200.0;
    ip.X = X;
    ip.totpoints = totpoints;
    s.ip = &ip;

    pntc_init(&s);

    //////////////////////////////////////////////////////////

    struct pntc_amplitude_params p;
    p.delta_tl = 0;
    p.verbose = false;

    struct pntc_boundary_state bs;
    dspin tj = 10;
    
    int i;
    for (i = 0; i < 10; i++) {
        bs.tjs[i] = tj;
    }

    // dspin ti = 4;
    // for (i = 0; i < 5; i++) {
    //     bs.tis[i] = ti;
    // }

    dspin ti1, ti2, ti3, ti4, ti5;
    double ampl;

    ti1 = 0;
    ti2 = 4;
    ti3 = 8;
    ti4 = 12;
    ti5 = 16;
    
    // Id
    bs.tis[0] = ti1;
    bs.tis[1] = ti2;
    bs.tis[2] = ti3;
    bs.tis[3] = ti4;
    bs.tis[4] = ti5;
    ampl = pntc_amplitude(&p, &bs);
    printf("\nId Amplitude: %g\n", ampl);

    // p1
    bs.tis[0] = ti5;
    bs.tis[1] = ti1;
    bs.tis[2] = ti2;
    bs.tis[3] = ti3;
    bs.tis[4] = ti4;
    ampl = pntc_amplitude(&p, &bs);
    printf("\np1 Amplitude: %g\n", ampl);

    // p2
    bs.tis[0] = ti4;
    bs.tis[1] = ti5;
    bs.tis[2] = ti1;
    bs.tis[3] = ti2;
    bs.tis[4] = ti3;
    ampl = pntc_amplitude(&p, &bs);
    printf("\np2 Amplitude: %g\n", ampl);

    // p3
    bs.tis[0] = ti3;
    bs.tis[1] = ti4;
    bs.tis[2] = ti5;
    bs.tis[3] = ti1;
    bs.tis[4] = ti2;
    ampl = pntc_amplitude(&p, &bs);
    printf("\np3 Amplitude: %g\n", ampl);

    // p4
    bs.tis[0] = ti2;
    bs.tis[1] = ti3;
    bs.tis[2] = ti4;
    bs.tis[3] = ti5;
    bs.tis[4] = ti1;
    ampl = pntc_amplitude(&p, &bs);
    printf("\np4 Amplitude: %g\n", ampl);

    // r1
    bs.tis[0] = ti1;
    bs.tis[1] = ti5;
    bs.tis[2] = ti4;
    bs.tis[3] = ti3;
    bs.tis[4] = ti2;
    ampl = pntc_amplitude(&p, &bs);
    printf("\nr1 Amplitude: %g\n", ampl);

    // r2
    bs.tis[0] = ti3;
    bs.tis[1] = ti2;
    bs.tis[2] = ti1;
    bs.tis[3] = ti5;
    bs.tis[4] = ti4;
    ampl = pntc_amplitude(&p, &bs);
    printf("\nr2 Amplitude: %g\n", ampl);

    // r3
    bs.tis[0] = ti5;
    bs.tis[1] = ti4;
    bs.tis[2] = ti3;
    bs.tis[3] = ti2;
    bs.tis[4] = ti1;
    ampl = pntc_amplitude(&p, &bs);
    printf("\nr3 Amplitude: %g\n", ampl);

    // r4
    bs.tis[0] = ti2;
    bs.tis[1] = ti1;
    bs.tis[2] = ti5;
    bs.tis[3] = ti4;
    bs.tis[4] = ti3;
    ampl = pntc_amplitude(&p, &bs);
    printf("\nr4 Amplitude: %g\n", ampl);

    // r5
    bs.tis[0] = ti4;
    bs.tis[1] = ti3;
    bs.tis[2] = ti2;
    bs.tis[3] = ti1;
    bs.tis[4] = ti5;
    ampl = pntc_amplitude(&p, &bs);
    printf("\nr5 Amplitude: %g\n", ampl);



    pntc_free();
    free(X);

}