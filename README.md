## Computing EPRL vertex transition amplitudes

This library aims at providing commands for computing the transition amplitudes
of the EPRL vertex.

---

### Compiling

For now only Linux systems are supported. You need to have `libwigxjpf`, `libgsl` and `glibc` installed. Run

```bash
make lib
```

to compile the library. The binary file will be saved into the `lib/` directory.


### Usage

Include the header `inc/pentachoron.h` into your project and link to the library with the `-lpntc` flag.

### Code conventions

Usually in function parameters all semi-integer spins are given as _integer double spins_. Nomenclature of parameters tries to make it clear by prepending the letter `t` for _two_ to all double spin parameters. 

For example, for computing `(-1)^j` for `j == 1` call the function `real_negpow(dspin tj)` with `tj == 2`.