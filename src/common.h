#ifndef __PENTACHORON_COMMON_H__
#define __PENTACHORON_COMMON_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdlib.h>
#include <inttypes.h>

#include "config.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Definitions and functions common to all files.
////////////////////////////////////////////////////////////////////////

// Returns the dimension of the spin-j representation.
#define DIM(tj) (tj + 1)

// Barbero-Immirzi constant.
extern double GAMMA;

// Machine type for representing integer spins.
// By conventions, all spin arguments to function are
// (positive) integers of the form 2 * spin.
// Nomenclature is as follows: a dspin variable that starts
// with 't' means that the variable holds 2 * spin value.
// Example: j == 0.5, tj == 1.
typedef int dspin;

// Machine type for half-integer spins.
typedef double spin;

// Bool types.
typedef enum { false, true } bool;



// Returns true if the library has been correctly initialized.
bool lib_initialized();

/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_COMMON_H__*/