#ifndef __PENTACHORON_CGAMMA_H__
#define __PENTACHORON_CGAMMA_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <mpc.h>

#include "common.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Computes the logarithm of complex Gamma function 
// in arbitrary precision (ab) using Lanczos methos.
////////////////////////////////////////////////////////////////////////

// Computes the logarithm of complex Gamma function of op and stores 
// result in rop.
int complex_lngamma(mpc_t rop, mpc_t op);

// Always call this before using complex gamma.
void init_complex_gamma();

// Always call this when finished using complex gamma.
void clear_complex_gamma();

/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_CGAMMA_H__*/