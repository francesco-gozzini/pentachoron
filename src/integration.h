#ifndef __PENTACHORON_INTEGRATION_H__
#define __PENTACHORON_INTEGRATION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"
#include "config.h"
#include <gsl/gsl_integration.h>

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Simple numerical integration routines.
// TODO: improve.
////////////////////////////////////////////////////////////////////////

// Always call this before using integration routines.
void init_integration(double gsl_epsrel, double ub, double* X, size_t npoints);

// Always call this when finished using integration routines.
void clear_integration();

// Wrapper for performing the integration for the B4
// boost coefficients.
double boost_integration(const gsl_function* f);

// Simplest possible integration on given mesh X of the function f.
double integrate_simple(const gsl_function* f, double* X, size_t npoints);

#if USE_ARBITRARY_PRECISION == 1 && PARALLELIZE

// Simple parallel integration.
double integrate_simple_parallel(const gsl_function* f, double* X, size_t npoints);

#endif

// Generates a uniformly spaced mesh from a to b of npoints points;
// there are (npoints-1) cells on the mesh.
void uniform_grid(double* X, double a, double b, size_t npoints);

// Generates a piecewise uniform mesh. The array xs contains the nodes
// of the piecewise mesh and the array npoints contains the number of 
// points per section. 
void piecewise_grid(double* X, double* xs, size_t* npoints, size_t nxs);
			
/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_INTEGRATION_H__*/