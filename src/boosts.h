#ifndef __PENTACHORON_BOOSTS_H__
#define __PENTACHORON_BOOSTS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Functions for computing the boosts coefficients B3 and B4.
////////////////////////////////////////////////////////////////////////

// known zero cases exploiting symmetries of the
// wigner 4jm symbols
#define check_zero_boost(tj1, tj2, tj3, tj4, tl1, tl2, tl3, tl4, ti, tk) \
    {   if ((ti + tk) % 4 != 0) { \
        if (tj1 == tj2 && tl1 == tl2 && tj1 == tl1) {return 0;} \
        if (tj3 == tj4 && tl3 == tl4 && tj3 == tl3) {return 0;} \
        if (tj1 == tj2 && tj3 == tj4 && tl1 == tl2) {return 0;} \
        if (tj1 == tj2 && tj3 == tj4 && tl3 == tl4) {return 0;} \
        if (tl1 == tl2 && tl3 == tl4 && tj1 == tj2) {return 0;} \
        if (tl1 == tl2 && tl3 == tl4 && tj3 == tj4) {return 0;} \
    }}

// Computes the boost B3 coefficients. 
double B3(dspin tj1, dspin tj2, dspin tj3,
          dspin tl1, dspin tl2, dspin tl3);

// Computes the boost B4 coefficients.
double B4(dspin tj1, dspin tj2, dspin tj3, dspin tj4,
          dspin tl1, dspin tl2, dspin tl3, dspin tl4,
          dspin ti, dspin tk);

// Integrand for the integration in the computation of
// the B4 coefficients. 
double B4_integrand(double r, void* params);
			
/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_BOOSTS_H__*/