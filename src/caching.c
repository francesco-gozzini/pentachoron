#include <time.h>

#include "caching.h"
#include "error.h"
#include "htbl.h"
#include "boosts.h"
#include "utils.h"

clock_t b4time;

double get_B4_from_cache(
          dspin tj1, dspin tj2, dspin tj3, dspin tj4,
          dspin tl1, dspin tl2, dspin tl3, dspin tl4,
          dspin ti, dspin tk) {

    double res;
    uint32_t key;

    key = b4_hash(tj1, tj2, tj3, tj4,
                  tl1, tl2, tl3, tl4,
                  ti, tk);

    if (
        b4_get_value(key, 
          tj1, tj2, tj3, tj4,
          tl1, tl2, tl3, tl4,
          ti, tk, &res)
    ) {
        return res;
    }

    // not found in cache
    // compute and insert eventually
    
    struct timespec start, stop;
    double dtime;

    clock_gettime(CLOCK_MONOTONIC, &start);

    res = B4(tj1, tj2, tj3, tj4,
             tl1, tl2, tl3, tl4,
             ti, tk);

    clock_gettime(CLOCK_MONOTONIC, &stop);
    dtime = (double)(stop.tv_sec - start.tv_sec);
    dtime += (double)(stop.tv_nsec - start.tv_nsec) / 1000000000.0;

    // insert (if needed)
    b4_insert(key,
              tj1, tj2, tj3, tj4,
              tl1, tl2, tl3, tl4,
              ti, tk, res, dtime);

    return res;

}