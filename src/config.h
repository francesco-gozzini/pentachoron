#ifndef __PENTACHORON_CONFIG_H__
#define __PENTACHORON_CONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Global configuration.
////////////////////////////////////////////////////////////////////////

// Enable/disable debug code.
#define DEBUG 1

// Enable/disable caching.
#define USE_CACHING 1

// Enable/disable arbitrary precision arithmetic.
#define USE_ARBITRARY_PRECISION 0

// Enable/disable parallelization.
#define PARALLELIZE 1

// Bits of precision for arbitrary precision mode.
#define PRECISION 128

// Use reducible of irreducible coupling.
#define USE_REDUCIBLE_COUPLING 0

/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_CONFIG_H__*/
