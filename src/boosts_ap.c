#include "common.h"

// compile this file if arbitrary precision is ENABLED
// by configuration
#if USE_ARBITRARY_PRECISION

#include <assert.h>
#include <complex.h>
#include <mpfr.h>
#include <mpc.h>

#include "boosts.h"
#include "njm.h"
#include "utils.h"
#include "njm.h"
#include "error.h"
#include "plot.h"
#include "integration.h"
#include "cgamma.h"

// Computes the k(j1, j2, j3) coefficient in the general case.
// Stores the result in the rop argument.
static void kappa(mpc_t rop, double r1, double r2, double r3,
                             dspin tk1, dspin tk2, dspin tk3,
                             dspin tj1, dspin tj2, dspin tj3) {

    // check for integers
    assert((tj1+tj2-tj3) % 2 == 0);
    assert((tj2+tj3-tj1) % 2 == 0);
    assert((tj1+tj3-tj2) % 2 == 0);
    assert((tj1+tj2+tj3) % 2 == 0);

    if (!triangle(tj1, tj2, tj3)) {
        mpc_set_d(rop, 0, MPC_RNDNN);
        return;
    }

    spin j1 = ((double)tj1)/2.0;
    spin j2 = ((double)tj2)/2.0;
    spin j3 = ((double)tj3)/2.0;

    spin k1 = ((double)tk1)/2.0;
    spin k2 = ((double)tk2)/2.0;
    spin k3 = ((double)tk3)/2.0;
    spin K = k1+k2+k3;

    mpc_t result;
    mpc_init2(result, PRECISION);

    // real ap vars
    mpfr_t x, y;
    mpfr_init2(x, PRECISION);
    mpfr_init2(y, PRECISION);

    // complex ap vars
    mpc_t z, w;
    mpc_init2(z, PRECISION);
    mpc_init2(w, PRECISION);

    // first factors in the product, before the sums
    mpc_set_dc(result, complex_negpow(-tk1-tk2 + tj1-tj2+tj3), MPC_RNDNN);
    
    mpfr_set_ui(x, (unsigned long int)(DIM(tj3)), MPFR_RNDN);
    mpfr_sqrt(y, x, MPFR_RNDN);
    mpc_div_fr(result, result, y, MPC_RNDNN);

    // phase part
    mpc_set_d_d(z, 1.0 + j3, r3, MPC_RNDNN);
    complex_lngamma(w, z);

    mpc_set_d_d(z, 1.0 + j1, r1, MPC_RNDNN);
    complex_lngamma(z, z);
    mpc_sub(w, w, z, MPC_RNDNN);

    mpc_set_d_d(z, 1.0 + j2, r2, MPC_RNDNN);
    complex_lngamma(z, z);
    mpc_sub(w, w, z, MPC_RNDNN);

    mpc_exp(w, w, MPC_RNDNN);

    mpc_abs(x, w, MPC_RNDNN);
    mpc_div_fr(w, w, x, MPC_RNDNN);

    mpc_mul(result, result, w, MPC_RNDNN);

    // check the factorial are non-negative
    assert((tj1 - tk1) >= 0);
    assert((tj2 + tk2) >= 0);
    assert((tj1 + tk1) >= 0);
    assert((tj2 - tk2) >= 0);

    // sqrt of factorial before the sums
    mpfr_fac_ui(x, (unsigned int)(tj1 - tk1)/2, MPFR_RNDN);
    mpfr_fac_ui(y, (unsigned int)(tj2 + tk2)/2, MPFR_RNDN);
    mpfr_mul(x, x, y, MPFR_RNDN);
    mpfr_fac_ui(y, (unsigned int)(tj1 + tk1)/2, MPFR_RNDN);
    mpfr_div(x, x, y, MPFR_RNDN);
    mpfr_fac_ui(y, (unsigned int)(tj2 - tk2)/2, MPFR_RNDN);
    mpfr_div(x, x, y, MPFR_RNDN);
    mpfr_sqrt(x, x, MPFR_RNDN);
    mpc_mul_fr(result, result, x, MPC_RNDNN);

    // now the sums remain ...
    mpc_t nsum, ssum, s1prod, s2prod;
    mpc_init2(nsum, PRECISION);
    mpc_init2(ssum, PRECISION);
    mpc_init2(s1prod, PRECISION);
    mpc_init2(s2prod, PRECISION);
    mpc_set_ui(nsum, 0, MPC_RNDNN);

    mpfr_t spref;
    mpfr_init2(spref, PRECISION);

    double wig;
    dspin tn, ts1, ts2;
    spin s1, s2;

    for (tn = -tj1; tn <= min(tj1, tk3 + tj2); tn += 2) {

        // check for allowed summands
        if (tj1-tn < 0) {
            continue;
        }
        if (tj2+tk3-tn < 0) {
            continue;
        }
        if (tj1+tn < 0) {
            continue;
        }
        if (tj2-tk3+tn < 0) {
            continue;
        }

        // wigner
        // TODO: I *REALLY* hope that I do not have to write
        //       an arbitrary precision routine also for the 
        //       3jm symbol... check T_T

        assert(wig_initialized());
        wig = wigner_3jm(
            tj1, tj2,    tj3,
            tn,  tk3-tn, -tk3
        );

        if (wig == 0) {
            continue;
        }

        // sqrt of factorial into sum over n
        
        mpfr_fac_ui(x, (unsigned int)(tj1 + tn)/2, MPFR_RNDN);
        mpfr_fac_ui(y, (unsigned int)(tj2 - tk3 + tn)/2, MPFR_RNDN);
        mpfr_mul(x, x, y, MPFR_RNDN);
    
        mpfr_fac_ui(y, (unsigned long int)(tj1 - tn)/2, MPFR_RNDN);
        mpfr_div(x, y, x, MPFR_RNDN);
        mpfr_fac_ui(y, (unsigned int)(tj2 + tk3 - tn)/2, MPFR_RNDN);
        mpfr_mul(x, x, y, MPFR_RNDN);

        mpfr_sqrt(spref, x, MPFR_RNDN);

        // multiply 3j
        mpfr_mul_d(spref, spref, wig, MPFR_RNDN);

        mpc_set_ui(ssum, 0, MPC_RNDNN);

        for (ts1 = max(tk1, tn); ts1 <= tj1; ts1 += 2) {

            s1 = ((double)ts1) / 2.0;

            // check for allowed summands
            if (tj1+ts1 < 0) {
                continue;
            }
            if (tj1-ts1 < 0) {
                continue;
            }
            if (ts1-tk1 < 0) {
                continue;
            }
            if (ts1-tn < 0) {
                continue;
            }

            mpfr_fac_ui(x, (unsigned int)(tj1-ts1)/2, MPFR_RNDN);
            mpfr_fac_ui(y, (unsigned int)(ts1-tk1)/2, MPFR_RNDN);
            mpfr_mul(x, x, y, MPFR_RNDN);
            mpfr_fac_ui(y, (unsigned int)(ts1-tn)/2, MPFR_RNDN);
            mpfr_mul(x, x, y, MPFR_RNDN);

            mpfr_fac_ui(y, (unsigned int)(tj1+ts1)/2, MPFR_RNDN);
            mpfr_div(x, y, x, MPFR_RNDN);

            mpc_set_fr(s1prod, x, MPC_RNDNN);

            mpc_set_d_d(z, (1.0-K+ts1)/2.0, -(r1-r2-r3)/2.0, MPC_RNDNN);
            complex_lngamma(w, z);
            
            mpc_set_d_d(z, 1.0+s1, -r1, MPC_RNDNN);
            complex_lngamma(z, z);
            mpc_sub(w, w, z, MPC_RNDNN);

            mpc_exp(w, w, MPC_RNDNN);

            mpc_mul(s1prod, s1prod, w, MPC_RNDNN);

            for (ts2 = max(-tk2, tn-tk3); ts2 <= tj2; ts2 += 2) {

                s2 = ((double)ts2) / 2.0;

                // check for allowed summands
                if (tj2+ts2 < 0) {
                    continue;
                }
                if (tj2-ts2 < 0) {
                    continue;
                }
                if (tk2+ts2 < 0) {
                    continue;
                }
                if (tk3-tn+ts2 < 0) {
                    continue;
                }

                mpc_set(s2prod, s1prod, MPC_RNDNN);

                mpc_set_dc(z, complex_negpow(ts1+ts2-tk1+tk2), MPC_RNDNN);
                mpc_mul(s2prod, s2prod, z, MPC_RNDNN);

                mpfr_fac_ui(x, (unsigned int)(tj2-ts2)/2, MPFR_RNDN);
                mpfr_fac_ui(y, (unsigned int)(ts2+tk2)/2, MPFR_RNDN);
                mpfr_mul(x, x, y, MPFR_RNDN);
                mpfr_fac_ui(y, (unsigned int)(tk3-tn+ts2)/2, MPFR_RNDN);
                mpfr_mul(x, x, y, MPFR_RNDN);

                mpfr_fac_ui(y, (unsigned int)(tj2+ts2)/2, MPFR_RNDN);
                mpfr_div(x, y, x, MPFR_RNDN);

                mpc_mul_fr(s2prod, s2prod, x, MPC_RNDNN);
                
                mpc_set_d_d(z, (1.0+K+ts2)/2.0, +(r1-r2+r3)/2.0, MPC_RNDNN);
                complex_lngamma(w, z);
                
                mpc_set_d_d(z, (1.0-k1+k2+k3-tn+ts1+ts2)/2.0, -(r1+r2-r3)/2.0, MPC_RNDNN);
                complex_lngamma(z, z);
                mpc_add(w, w, z, MPC_RNDNN);

                mpc_set_d_d(z, 1.0+s2, -r2, MPC_RNDNN);
                complex_lngamma(z, z);
                mpc_sub(w, w, z, MPC_RNDNN);
                
                mpc_set_d_d(z, 1.0+s1+s2, r3, MPC_RNDNN);
                complex_lngamma(z, z);
                mpc_sub(w, w, z, MPC_RNDNN);

                mpc_exp(w, w, MPC_RNDNN);
                mpc_mul(s2prod, s2prod, w, MPC_RNDNN);

                mpc_add(ssum, ssum, s2prod, MPC_RNDNN);

            }
        }

        mpc_set_d_d(z, (1.0-k1+k2+k3-tn)/2.0, -(r1+r2+r3)/2.0, MPC_RNDNN);
        complex_lngamma(w, z);
        mpc_exp(w, w, MPC_RNDNN);
        mpc_div(ssum, ssum, w, MPC_RNDNN);

        mpc_mul_fr(ssum, ssum, spref, MPC_RNDNN);
        mpc_add(nsum, nsum, ssum, MPC_RNDNN);

    } 

    mpc_mul(result, result, nsum, MPC_RNDNN);
    mpc_set(rop, result, MPC_RNDNN);

    // clear variables
    mpfr_clears(x, y, spref, (mpfr_ptr) 0);
    mpc_clear(result);
    mpc_clear(z);
    mpc_clear(w);
    mpc_clear(nsum);
    mpc_clear(ssum);
    mpc_clear(s1prod);
    mpc_clear(s2prod);

}

double B4_integrand(double r, void* params) {

    dspin* tjs = (dspin*)params;

    dspin tj1 = tjs[0];
    dspin tj2 = tjs[1];
    dspin tj3 = tjs[2];
    dspin tj4 = tjs[3];
    dspin tl1 = tjs[4];
    dspin tl2 = tjs[5];
    dspin tl3 = tjs[6];
    dspin tl4 = tjs[7];
    dspin ti  = tjs[8];
    dspin tk  = tjs[9];

    spin j1 = ((double)(tj1)) / 2.0;
    spin j2 = ((double)(tj2)) / 2.0;
    spin j3 = ((double)(tj3)) / 2.0;
    spin j4 = ((double)(tj4)) / 2.0;

    dspin TJ = tj1 + tj2 + tj3 + tj4;
    dspin TL = tl1 + tl2 + tl3 + tl4;

    spin J = TJ / 2;
    spin L = TL / 2;

    mpfr_t hsum, hpr, x, y, pi;
    mpfr_init2(hsum, PRECISION);
    mpfr_init2(hpr, PRECISION);
    mpfr_init2(x, PRECISION);
    mpfr_init2(y, PRECISION);
    mpfr_init2(pi, PRECISION);

    mpc_t hpc, z;
    mpc_init2(hpc, PRECISION);
    mpc_init2(z, PRECISION);

    mpfr_set_ui(hsum, 0, MPFR_RNDN);
    mpfr_const_pi(pi, MPFR_RNDN);
    
    // external phase factor
    double complex eph;
    eph = complex_negpow((dspin)(J-L)) * complex_negpow(ti-tk);

    int tm = min(ti, tk);
    dspin th;
    spin h;
    for (th = -tm; th <= tm; th += 2) {

        check_triangle_continue(tj1, tj2, ti);
        check_triangle_continue(tl1, tl2, tk);
        check_triangle_continue(tj3, tj4, ti);
        check_triangle_continue(tl3, tl4, tk);

        h = (double)th / 2.0;

        mpc_set_dc(hpc, eph, MPC_RNDNN);

        kappa(z, GAMMA*j1, GAMMA*j2, r, 
                 tj1, tj2, th,
                 tj1, tj2, ti);
        mpc_conj(z, z, MPC_RNDNN);
        mpc_mul(hpc, hpc, z, MPC_RNDNN);

        kappa(z, GAMMA*j1, GAMMA*j2, r, 
                 tj1, tj2, th,
                 tl1, tl2, tk);
        mpc_mul(hpc, hpc, z, MPC_RNDNN);

        kappa(z, GAMMA*j3, GAMMA*j4, r, 
                 tj3, tj4, th,
                 tj3, tj4, ti);
        mpc_conj(z, z, MPC_RNDNN);
        mpc_mul(hpc, hpc, z, MPC_RNDNN);

        kappa(z, GAMMA*j3, GAMMA*j4, r, 
                 tj3, tj4, th,
                 tl3, tl4, tk);
        mpc_mul(hpc, hpc, z, MPC_RNDNN);

        // now hpc should be real, take the real part
        // TODO: check reality

        mpc_real(hpr, hpc, MPFR_RNDN);
        mpfr_mul_d(hpr, hpr, r*r + h*h, MPFR_RNDN);

        mpfr_mul_d(x, pi, j1 + j2 + h, MPFR_RNDN);
        mpfr_cos(y, x, MPFR_RNDN);

        mpfr_mul_d(x, pi, GAMMA*j1 + GAMMA*j2 + r, MPFR_RNDN);
        mpfr_cosh(x, x, MPFR_RNDN);

        mpfr_add(y, x, y, MPFR_RNDN);
        mpfr_div(hpr, hpr, y, MPFR_RNDN);

        mpfr_mul_d(x, pi, j3 + j4 + h, MPFR_RNDN);
        mpfr_cos(y, x, MPFR_RNDN);

        mpfr_mul_d(x, pi, GAMMA*j3 + GAMMA*j4 + r, MPFR_RNDN);
        mpfr_cosh(x, x, MPFR_RNDN);

        mpfr_add(y, x, y, MPFR_RNDN);
        mpfr_div(hpr, hpr, y, MPFR_RNDN);

        mpfr_add(hsum, hsum, hpr, MPFR_RNDN);

    }

    double result;
    result = mpfr_get_d(hsum, MPFR_RNDN);

    // clear variables
    mpfr_clears(x, y, hsum, hpr, pi, (mpfr_ptr) 0);
    mpc_clear(hpc);
    mpc_clear(z);
    
    // truncate to double
    return result;

}

double B4(dspin tj1, dspin tj2, dspin tj3, dspin tj4,
          dspin tl1, dspin tl2, dspin tl3, dspin tl4,
          dspin ti, dspin tk) {

    // check for mutual integrity/semi-integrity
    // of spins (comes from jappa coefficients)
    check_integer_2sum_return0(tj1, tl1);
    check_integer_2sum_return0(tj2, tl2);
    check_integer_2sum_return0(tj3, tl3);
    check_integer_2sum_return0(tj4, tl4);

    // check for mutual integrity/semi-integrity
    // of spins - intertwiner pairs
    // (comes from 4jm symbols)
    check_integer_3sum_return0(tj1, tj2, ti);
    check_integer_3sum_return0(tj3, tj4, ti);
    check_integer_3sum_return0(tl1, tl2, tk);
    check_integer_3sum_return0(tl3, tl4, tk);

    // test for trinagular inequalities
    check_triangle_return0(tj1, tj2, ti);
    check_triangle_return0(tj3, tj4, ti);
    check_triangle_return0(tl1, tl2, tk);
    check_triangle_return0(tl3, tj4, tk);

    // test for known zero cases
    check_zero_boost(tj1, tj2, tj3, tj4, tl1, tl2, tl3, tl4, ti, tk);

    double result, factor;
    
    factor = M_PI * M_PI / 32.0 * DIM(ti) * DIM(tk)
            * sqrt(DIM(tj1) * DIM(tl1))
            * sqrt(DIM(tj2) * DIM(tl2))
            * sqrt(DIM(tj3) * DIM(tl3))
            * sqrt(DIM(tj4) * DIM(tl4));

    dspin params[10] = {tj1, tj2, tj3, tj4, tl1, tl2, tl3, tl4, ti, tk};

    gsl_function F;
    F.function = &B4_integrand;
    F.params = params;

    // perform integration
    double integral = boost_integration(&F);

    result = factor * integral;
    return result;

}

double B3(dspin tj1, dspin tj2, dspin tj3,
          dspin tl1, dspin tl2, dspin tl3) {

    // check for J, L integers
    assert((tj1+tj2+tj3) % 2 == 0);
    assert((tl1+tl2+tl3) % 2 == 0);

    spin j1 = ((double)tj1)/2.0;
    spin j2 = ((double)tj2)/2.0;
    spin j3 = ((double)tj3)/2.0;

    dspin TJ = tj1 + tj2 + tj3;
    dspin TL = tl1 + tl2 + tl3;

    spin J = TJ / 2;
    spin L = TL / 2;

    double P = ((double)TJ)/2.0*GAMMA;
    double K = ((double)TL)/2.0;

    double factor;
    factor = M_PI / 16.0
            * sqrt(DIM(tj1) * DIM(tl1))
            * sqrt(DIM(tj2) * DIM(tl2))
            * (DIM(tj3) * DIM(tl3));

    mpc_t z, w;
    mpc_init2(z, PRECISION);
    mpc_init2(w, PRECISION);

    mpfr_t res, x, y;
    mpfr_init2(res, PRECISION);
    mpfr_init2(x, PRECISION);
    mpfr_init2(y, PRECISION);

    mpc_set_dc(z, complex_negpow((dspin)(J-L)), MPC_RNDNN);
    
    kappa(w, GAMMA*j1, GAMMA*j2, GAMMA*j3,
             tj1, tj2, tj3,
             tj1, tj2, tj3);
    mpc_conj(w, w, MPC_RNDNN);
    mpc_mul(z, z, w, MPC_RNDNN);

    kappa(w, GAMMA*j1, GAMMA*j2, GAMMA*j3,
             tj1, tj2, tj3,
             tl1, tl2, tl3);
    mpc_mul(z, z, w, MPC_RNDNN);

    // now z should be real, take the real part
    // (it's not exactly real it always has a small imaginary part...)

    mpc_real(res, z, MPC_RNDNN);
    mpfr_mul_d(res, res, factor, MPFR_RNDN);

    mpfr_const_pi(x, MPFR_RNDN);
    mpfr_mul_d(x, x, K, MPFR_RNDN);
    mpfr_cos(y, x, MPFR_RNDN);

    mpfr_const_pi(x, MPFR_RNDN);
    mpfr_mul_d(x, x, P, MPFR_RNDN);
    mpfr_cosh(x, x, MPFR_RNDN);

    mpfr_add(y, x, y, MPFR_RNDN);
    mpfr_div(res, res, y, MPFR_RNDN);

    double result;
    result = mpfr_get_d(res, MPFR_RNDN);

    // clear variables
    mpfr_clears(x, y, res, (mpfr_ptr) 0);
    mpc_clear(z);
    mpc_clear(w);

    // truncate to double
    return result;

}

#endif // #if USE_ARBITRARY_PRECISION == 1
