#include <omp.h>

#include "njm.h"
#include "utils.h"
#include "wigxjpf.h"
#include "config.h"

#define MAX_J 200

bool was_wig_initialized = false;

void init_wigxjpf() {

	// TODO: consider use FASTWIGXJPF for fast computation
	//       of 6js with hash tables

	// TODO: for maximum performance also the 15js should
	//       be precomputed and stored in RAM. An hash table
	//       similar to the B4s can be used. Ideally generalize
	//       the capped hash table of the B4

	wig_table_init(2*MAX_J, 9);
	wig_temp_init(2*MAX_J);
	was_wig_initialized = true;

}

void init_wigxjpf_thread() {

	int tid = omp_get_thread_num();
	if (tid != 0) {
		// not in master thread
		wig_thread_temp_init(2*MAX_J);
	}
	
}

void clear_wigxjpf_thread() {

	int tid = omp_get_thread_num();
	if (tid != 0) {
		// not in master thread
		wig_temp_free();
	}
	
}

void clear_wigxjpf() {

	wig_temp_free();
	wig_table_free();
	was_wig_initialized = false;

}

bool wig_initialized() {
	return was_wig_initialized;
}

double wigner_15j_reducible(dspin ti1, dspin ti2, dspin ti3, dspin ti4, dspin ti5,
		    dspin tj1, dspin tj2, dspin tj3, dspin tj4, dspin tj5,
		    dspin tj6, dspin tj7, dspin tj8, dspin tj9, dspin tj10) {

	// use splitting into two 6j and one 9j by Giorgio thesis

	dspin sum = 2*(tj4+tj3+ti1)-(ti2+ti3+ti4+ti5);
	int sign = real_negpow(sum);

	double sj1, sj2, nj;

	sj1 = wig6jj(ti1, ti3, ti2, tj1, tj2, tj3);
	if (sj1 == 0) { return 0; }
	sj2 = wig6jj(ti1, ti4, ti5, tj6, tj4, tj5);
	if (sj2 == 0) { return 0; }
	nj = wig9jj(ti2, ti3, ti1, tj10, tj9, ti4, tj7, tj8, ti5);
	if (nj == 0) { return 0; }

	return sign*sj1*sj2*nj;

}

double wigner_15j(dspin tj1, dspin tj2, dspin tj3, dspin tj4, dspin tj5,
		          dspin tl1, dspin tl2, dspin tl3, dspin tl4, dspin tl5,
		          dspin tk1, dspin tk2, dspin tk3, dspin tk4, dspin tk5) {

	// TODO: a lot of possible optimizations here
	//       for example there are a lot of possible additional bounds on tx
	//       and there are a lot of triangular inequalities to check
	//       and a lot of symmetries to exploit...

	check_triangle_return0(tj1, tj2, tl1);
	check_triangle_return0(tj2, tj3, tl2);
	check_triangle_return0(tj3, tj4, tl3);
	check_triangle_return0(tj4, tj5, tl4);
	check_triangle_return0(tj5, tk1, tl5);

	check_triangle_return0(tk2, tk1, tl1);
	check_triangle_return0(tk3, tk2, tl2);
	check_triangle_return0(tk4, tk3, tl3);
	check_triangle_return0(tk5, tk4, tl4);
	check_triangle_return0(tj1, tk5, tl5);

	check_integer_3sum_return0(tj1, tj2, tl1);
	check_integer_3sum_return0(tj2, tj3, tl2);
	check_integer_3sum_return0(tj3, tj4, tl3);
	check_integer_3sum_return0(tj4, tj5, tl4);
	check_integer_3sum_return0(tj5, tk1, tl5);

	check_integer_3sum_return0(tk2, tk1, tl1);
	check_integer_3sum_return0(tk3, tk2, tl2);
	check_integer_3sum_return0(tk4, tk3, tl3);
	check_integer_3sum_return0(tk5, tk4, tl4);
	check_integer_3sum_return0(tj1, tk5, tl5);

	int mintx, maxtx;

	mintx = max(abs(tj1-tk1), abs(tj2-tk2));
	mintx = max(mintx, abs(tj3-tk3));
	mintx = max(mintx, abs(tj4-tk4));
	mintx = max(mintx, abs(tj5-tk5));

	maxtx = min(tj1+tk1, tj2+tk2);
	maxtx = min(maxtx, tj3+tk3);
	maxtx = min(maxtx, tj4+tk4);
	maxtx = min(maxtx, tj5+tk5);

	if (maxtx < mintx) {
		return 0;
	}

	double sj1, sj2, sj3, sj4, sj5;
	double res, c, y, t;
	int tx;

	res = 0.0;
	c = 0.0;
	for (tx = mintx; tx <= maxtx; tx += 2) {

		sj1 = wig6jj(tj1, tk1, tx, tk2, tj2, tl1);
		if (sj1 == 0) { continue; }
		sj2 = wig6jj(tj2, tk2, tx, tk3, tj3, tl2);
		if (sj2 == 0) { continue; }
		sj3 = wig6jj(tj3, tk3, tx, tk4, tj4, tl3);
		if (sj3 == 0) { continue; }
		sj4 = wig6jj(tj4, tk4, tx, tk5, tj5, tl4);
		if (sj4 == 0) { continue; }
		sj5 = wig6jj(tj5, tk5, tx, tj1, tk1, tl5);
		if (sj5 == 0) { continue; }

		// compensated summation
		comp_sum(DIM(tx) * sj1 * sj2 * sj3 * sj4 * sj5, res, c, y, t);

	}

	int sign = real_negpow(tj1 + tl1 + tk1 + tj2 + tl2 + tk2 +
						   tj3 + tl3 + tk3 + tj4 + tl4 + tk4 + tj5 + tl5 + tk5);
	return sign * res;

}

double wigner_3jm(
			dspin tj1, dspin tj2, dspin tj3,
			dspin tm1, dspin tm2, dspin tm3) {

	return wig3jj(tj1, tj2, tj3, tm1, tm2, tm3);

}
