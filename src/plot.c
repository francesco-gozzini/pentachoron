#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "plot.h"
#include "utils.h"

double* dbg_X;
double* dbg_Y;
size_t dbg_collected;
size_t dbg_npoints;
bool then_exit;
bool dbg_done = false;

void print_points(double* X, double* Y, size_t npoints) {

    printf("X = [ ");

    size_t i;
    for (i = 0; i < npoints; i++) {

        printf("%g ", X[i]);

    }
    printf("];\n");

    printf("Y = [ ");
    for (i = 0; i < npoints; i++) {

        printf("%g ", Y[i]);

    }

    printf("];\n");

}

void dbg_start_plot(size_t np, bool ex) {

    dbg_npoints = np;
    dbg_collected = 0;
    dbg_X = (double*) calloc((uint)np, sizeof(double));
    dbg_Y = (double*) calloc((uint)np, sizeof(double));
    then_exit = ex;

}

void dbg_collect_point(double x, double y) {

    if (dbg_done) {
        return;
    }

    dbg_X[dbg_collected] = x;
    dbg_Y[dbg_collected] = y;
    dbg_collected++;

    if (dbg_collected == dbg_npoints) {
        dbg_stop_plot();
    }

}

void dbg_stop_plot() {

    print_points(dbg_X, dbg_Y, dbg_collected);

    free(dbg_X);
    free(dbg_Y);

    dbg_done = true;

    // kills the program if requested
    if (then_exit) {
        exit(EXIT_SUCCESS);
    }
    
}

void plot(const gsl_function* f, double* X, size_t npoints) {

    double Y[npoints];
    size_t i;

    for (i = 0; i < npoints; i++) {

#if DEBUG == 1

        clock_t ftime;
        START_TIMING(ftime);
        Y[i] = f->function(X[i], f->params);
        STOP_TIMING(ftime);
        PRINT_TIMING(ftime);
        printf("Computing point #%zu ... Y = %g\n", i, Y[i]);

#else

        Y[i] = f->function(X[i], f->params);

#endif

    }
    
    print_points(X, Y, npoints);

}