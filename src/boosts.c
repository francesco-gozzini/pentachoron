#include "common.h"

// compile this file if arbitrary precision is DISABLED
// by configuration
#if !USE_ARBITRARY_PRECISION

#include <math.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_result.h>
#include <assert.h>
#include <complex.h> 

#include "boosts.h"
#include "njm.h"
#include "utils.h"
#include "njm.h"
#include "error.h"
#include "plot.h"
#include "integration.h"

// Computes the k(j1, j2, j3) coefficient in the simplified case
// with ri = GAMMA*ji, ki = ji.
// FIXME: I have to check for signs and also complex prefactor
//        disable compilation for now
#if 0
static double complex kappa_simple(dspin tj1, dspin tj2, dspin tj3) {

    // check for integers
    assert((tj1+tj2-tj3) % 2 == 0);
    assert((tj2+tj3-tj1) % 2 == 0);
    assert((tj1+tj3-tj2) % 2 == 0);
    assert((tj1+tj2+tj3) % 2 == 0);

    int a12 = (tj1+tj2-tj3)/2;
    int a23 = (tj2+tj3-tj1)/2;
    int a13 = (tj1+tj3-tj2)/2;

    if (a12 < 0 || a23 < 0 || a13 < 0) {
        return 0;
    }

    spin j1 = ((double)tj1)/2.0;
    spin j2 = ((double)tj2)/2.0;
    spin j3 = ((double)tj3)/2.0;
    spin J = j1+j2+j3;

    double Dji = sqrt(gsl_sf_fact((unsigned int)tj1) 
            * gsl_sf_fact((unsigned int)tj2) 
            * gsl_sf_fact((unsigned int)tj3)
            / gsl_sf_fact((unsigned int)(tj1+tj2+tj3)/2 + 1)
            / gsl_sf_fact((unsigned int)a12) 
            / gsl_sf_fact((unsigned int)a23) 
            / gsl_sf_fact((unsigned int)a13));

    int status;

    gsl_sf_result tmod;
    gsl_sf_result targ;

    // first factors in the product, before the sums

    double pref;
    double glnmod = 0.0;
    double ephase = 0.0; // external phase part

    pref = real_negpow(tj1-tj2+tj3)
           * Dji * gsl_sf_fact((unsigned int)a12) / sqrt(DIM(tj3));

    status = gsl_sf_lngamma_complex_e(1.0+j1, GAMMA*j1, &tmod, &targ);
    check_gsl(status);
    ephase -= targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j2, GAMMA*j2, &tmod, &targ);
    check_gsl(status);
    ephase -= targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j3, GAMMA*j3, &tmod, &targ);
    check_gsl(status);
    ephase += targ.val;

    // other two gammas before the sums 
    status = gsl_sf_lngamma_complex_e((1.0-a23)/2.0, GAMMA*a23/2.0, &tmod, &targ);
    check_gsl(status);
    glnmod += tmod.val;
    ephase += targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j1, -GAMMA*j1, &tmod, &targ);
    check_gsl(status);
    glnmod -= tmod.val;
    ephase -= targ.val;

    // add the two gammas
    pref *= exp(glnmod);

    // now the sums remain ...

    double sumreal, sumimag, nreal, nimag, smod, sarg, nfact;
    dspin tn, ts;
    spin n, s;

    sumreal = 0.0;
    sumimag = 0.0;
    for (tn = -tj1; tn <= tj1; tn += 2) {

        n = ((double)tn) / 2.0;

        // check for allowed summands
        if (tj2+tj3-tn < 0) {
            continue;
        }
        if (tj1-tn < 0) {
            continue;
        }
        if (tj2-tj3+tn < 0) {
            continue;
        }

        nfact = real_negpow(tj1+tn)
                * gsl_sf_fact((unsigned int)(tj2+tj3-tn)/2)
                / gsl_sf_fact((unsigned int)(tj1-tn)/2)
                / gsl_sf_fact((unsigned int)(tj2-tj3+tn)/2);

        nreal = 0.0;
        nimag = 0.0;
        for (ts = tn-tj3; ts <= tj2; ts += 2) {

            s = ((double)ts) / 2.0;

            // check for allowed summands
            if (tj2-ts < 0) {
                continue;
            }
            if (tj3-tn+ts < 0) {
                continue;
            }
            
            smod = real_negpow(tj2+ts)
                   / gsl_sf_fact((unsigned int)(tj2-ts)/2)
                   / gsl_sf_fact((unsigned int)(tj3-tn+ts)/2);

            sarg = 0.0;
            glnmod = 0.0;

            status = gsl_sf_lngamma_complex_e((1.0+J)/2.0 + s - n, -(GAMMA*a12)/2.0, &tmod, &targ);
            check_gsl(status);
            glnmod += tmod.val;
            sarg += targ.val;

            status = gsl_sf_lngamma_complex_e((1.0+J)/2.0 + s, (GAMMA*a13)/2.0, &tmod, &targ);
            check_gsl(status);
            glnmod += tmod.val;
            sarg += targ.val;

            status = gsl_sf_lngamma_complex_e((1.0+a23)/2.0 - n, -(GAMMA*J)/2.0, &tmod, &targ);
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            status = gsl_sf_lngamma_complex_e(1.0 + s, -GAMMA*j2, &tmod, &targ);
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            status = gsl_sf_lngamma_complex_e(1.0 + j1 + s, GAMMA*j3, &tmod, &targ);
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            // add global phase from outside
            sarg += ephase;

            smod *= exp(glnmod);
            nreal += smod*cos(sarg);
            nimag += smod*sin(sarg);

        }

        sumreal += nfact*nreal;
        sumimag += nfact*nimag;

    } 

    double complex result;
    result = pref * sumreal + I * pref * sumimag;

    // finally add possible complex phase
    // TODO: this is not in the original paper
    //       re-check if it's correct
    //result *= complex_negpow(tj3);

    return result;

}
#endif

// Computes the k(j1, j2, j3) coefficient in the a-bit-simplified case
// with r1,2 = GAMMA*j1,2, k1,2 = j1,2.
// FIXME: there is probably a sign error somewhere
//        disable compilation for now
#if 0
static double complex kappa_semi_simple(double r3, dspin tk3,
                     dspin tj1, dspin tj2, dspin tj3) {

    // check for integers
    assert((tj1+tj2-tj3) % 2 == 0);
    assert((tj2+tj3-tj1) % 2 == 0);
    assert((tj1+tj3-tj2) % 2 == 0);
    assert((tj1+tj2+tj3) % 2 == 0);

    spin j1 = ((double)tj1)/2.0;
    spin j2 = ((double)tj2)/2.0;
    spin j3 = ((double)tj3)/2.0;

    spin k3 = ((double)tk3)/2.0;
    spin K = j1+j2+k3;

    double r1 = GAMMA*j1;
    double r2 = GAMMA*j2;

    int status;

    double pref;
    double glnmod;

    gsl_sf_result tmod;
    gsl_sf_result targ;

    // first factors in the product, before the sums
    pref = real_negpow(tj1-tj2+tj3) / sqrt(DIM(tj3));

    // phase part
    double ephase = 0.0;

    status = gsl_sf_lngamma_complex_e(1.0+j1, r1, &tmod, &targ);
    check_gsl(status);
    ephase -= targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j2, r2, &tmod, &targ);
    check_gsl(status);
    ephase -= targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j3, r3, &tmod, &targ);
    check_gsl(status);
    ephase += targ.val;

    // sqrt factorial before the sums
    pref *= sqrt(gsl_sf_fact((unsigned int)(tj1))
               * gsl_sf_fact((unsigned int)(tj2))      
    );

    // now the sums remain ...

    double sumreal, sumimag, nreal, nimag, smod, sarg, nfact;
    dspin tn, ts;
    spin s;

    sumreal = 0.0;
    sumimag = 0.0;
    for (tn = -tj1; tn <= min(tj1, tk3 + tj2); tn += 2) {

        // check for allowed summands
        if (tj1-tn < 0) {
            continue;
        }
        if (tj2+tk3-tn < 0) {
            continue;
        }
        if (tj1+tn < 0) {
            continue;
        }
        if (tj2-tk3+tn < 0) {
            continue;
        }

        nfact = sqrt( gsl_sf_fact((unsigned int)(tj2 + tk3 - tn)/2)
            / gsl_sf_fact((unsigned int)(tj1 - tn)/2)
            / gsl_sf_fact((unsigned int)(tj1 + tn)/2)
            / gsl_sf_fact((unsigned int)(tj2 - tk3 + tn)/2)
        );

        // wigner
        assert(wig_initialized());

        nfact *= wigner_3jm(
            tj1, tj2,    tj3,
            tn,  tk3-tn, -tk3
        );

        nreal = 0.0;
        nimag = 0.0;

        for (ts = max(-tj2, tn-tk3); ts <= tj2; ts += 2) {

            s = ((double)ts) / 2.0;

            // check for allowed summands
            if (tj2 + ts < 0) {
                continue;
            }
            if (tj2 - ts < 0) {
                continue;
            }
            if (tk3-tn+ts < 0) {
                continue;
            }
            
            smod = real_negpow(ts+tj2)
                / gsl_sf_fact((unsigned int)(tj2-ts)/2)
                / gsl_sf_fact((unsigned int)(tk3-tn+ts)/2);

            sarg = 0.0;
            glnmod = 0.0;

            status = gsl_sf_lngamma_complex_e(
                (1.0-K+tj1)/2.0, -(r1-r2-r3)/2.0, &tmod, &targ
            );
            check_gsl(status);
            glnmod += tmod.val;
            sarg += targ.val;

            status = gsl_sf_lngamma_complex_e(
                (1.0+K+ts)/2.0, +(r1-r2+r3)/2.0, &tmod, &targ
            );
            check_gsl(status);
            glnmod += tmod.val;
            sarg += targ.val;

            status = gsl_sf_lngamma_complex_e(
                (1.0-j1+j2+k3-tn+tj1+ts)/2.0, -(r1+r2-r3)/2.0, &tmod, &targ
            );
            check_gsl(status);
            glnmod += tmod.val;
            sarg += targ.val;

            status = gsl_sf_lngamma_complex_e(
                1.0+j1, -r1, &tmod, &targ
            );
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            status = gsl_sf_lngamma_complex_e(
                1.0+s, -r2, &tmod, &targ
            );
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            status = gsl_sf_lngamma_complex_e(
                1.0+j1+s, r3, &tmod, &targ
            );
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            status = gsl_sf_lngamma_complex_e(
                (1.0-j1+j2+k3-tn)/2.0, -(r1+r2+r3)/2.0, &tmod, &targ
            );
            check_gsl(status);
            glnmod -= tmod.val;
            sarg -= targ.val;

            // add global phase from outside
            sarg += ephase;

            smod *= exp(glnmod);
            nreal += smod*cos(sarg);
            nimag += smod*sin(sarg);

        }

        sumreal += nfact*nreal;
        sumimag += nfact*nimag;

    } 

    double complex result;
    result = pref * sumreal + I * pref * sumimag;

    // finally add possible complex phase
    result *= complex_negpow(tj3);
    
    return result;

}
#endif

// Computes the k(j1, j2, j3) coefficient in the general case.
static double complex kappa(double r1, double r2, double r3,
                     dspin tk1, dspin tk2, dspin tk3,
                     dspin tj1, dspin tj2, dspin tj3) {

    // check for integers
    assert((tj1+tj2+tj3) % 2 == 0);

    if (!triangle(tj1, tj2, tj3)) {
        return 0.0;
    }

    spin j1 = ((double)tj1)/2.0;
    spin j2 = ((double)tj2)/2.0;
    spin j3 = ((double)tj3)/2.0;

    spin k1 = ((double)tk1)/2.0;
    spin k2 = ((double)tk2)/2.0;
    spin k3 = ((double)tk3)/2.0;
    spin K = k1+k2+k3;

    int status;
    double glnmod;

    gsl_sf_result tmod;
    gsl_sf_result targ;

    // phase part
    double ephase = 0.0;

    status = gsl_sf_lngamma_complex_e(1.0+j1, r1, &tmod, &targ);
    check_gsl(status);
    ephase -= targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j2, r2, &tmod, &targ);
    check_gsl(status);
    ephase -= targ.val;

    status = gsl_sf_lngamma_complex_e(1.0+j3, r3, &tmod, &targ);
    check_gsl(status);
    ephase += targ.val;

    // check the factorial are non-negative
    assert((tj1 - tk1) >= 0);
    assert((tj2 + tk2) >= 0);
    assert((tj1 + tk1) >= 0);
    assert((tj2 - tk2) >= 0);

    // now the sums remain ...
    double smod, sarg, nfact, wig;
    double complex sum, ncompl;
    dspin tn, ts1, ts2;
    spin s1, s2;

    sum = 0.0;
    for (tn = -tj1; tn <= min(tj1, tk3 + tj2); tn += 2) {

        // check for allowed summands
        if (tj1-tn < 0) {
            continue;
        }
        if (tj2+tk3-tn < 0) {
            continue;
        }
        if (tj1+tn < 0) {
            continue;
        }
        if (tj2-tk3+tn < 0) {
            continue;
        }

        // wigner
        assert(wig_initialized());

        wig = wigner_3jm(
            tj1, tj2,    tj3,
            tn,  tk3-tn, -tk3
        );

        if (wig == 0) {
            continue;
        }

        nfact = sqrt(
            gsl_sf_fact((unsigned int)(tj1 - tn)/2)
            * gsl_sf_fact((unsigned int)(tj2 + tk3 - tn)/2)
            / gsl_sf_fact((unsigned int)(tj1 + tn)/2)
            / gsl_sf_fact((unsigned int)(tj2 - tk3 + tn)/2)
        );

        nfact = nfact * wig;

        ncompl = 0.0;
        for (ts1 = max(tk1, tn); ts1 <= tj1; ts1 += 2) {

            s1 = ((double)ts1) / 2.0;

            // check for allowed summands
            if (tj1+ts1 < 0) {
                continue;
            }
            if (tj1-ts1 < 0) {
                continue;
            }
            if (ts1-tk1 < 0) {
                continue;
            }
            if (ts1-tn < 0) {
                continue;
            }

            for (ts2 = max(-tk2, tn-tk3); ts2 <= tj2; ts2 += 2) {

                s2 = ((double)ts2) / 2.0;

                // check for allowed summands
                if (tj2+ts2 < 0) {
                    continue;
                }
                if (tj2-ts2 < 0) {
                    continue;
                }
                if (tk2+ts2 < 0) {
                    continue;
                }
                if (tk3-tn+ts2 < 0) {
                    continue;
                }
                
                smod = gsl_sf_fact((unsigned int)(tj1+ts1)/2)
                     * gsl_sf_fact((unsigned int)(tj2+ts2)/2)
                     / (
                        gsl_sf_fact((unsigned int)(tj1-ts1)/2)
                        * gsl_sf_fact((unsigned int)(ts1-tk1)/2)
                        * gsl_sf_fact((unsigned int)(ts1-tn)/2)
                        * gsl_sf_fact((unsigned int)(tj2-ts2)/2)
                        * gsl_sf_fact((unsigned int)(ts2+tk2)/2)
                        * gsl_sf_fact((unsigned int)(tk3-tn+ts2)/2)
                     );

                sarg = 0.0;
                glnmod = 0.0;

                status = gsl_sf_lngamma_complex_e(
                    (1.0-K+ts1)/2.0, -(r1-r2-r3)/2.0, &tmod, &targ
                );
                check_gsl(status);
                glnmod += tmod.val;
                sarg += targ.val;

                status = gsl_sf_lngamma_complex_e(
                    (1.0+K+ts2)/2.0, +(r1-r2+r3)/2.0, &tmod, &targ
                );
                check_gsl(status);
                glnmod += tmod.val;
                sarg += targ.val;

                status = gsl_sf_lngamma_complex_e(
                    (1.0-k1+k2+k3-tn+ts1+ts2)/2.0, -(r1+r2-r3)/2.0, &tmod, &targ
                );
                check_gsl(status);
                glnmod += tmod.val;
                sarg += targ.val;

                status = gsl_sf_lngamma_complex_e(
                    1.0+s1, -r1, &tmod, &targ
                );
                check_gsl(status);
                glnmod -= tmod.val;
                sarg -= targ.val;

                status = gsl_sf_lngamma_complex_e(
                    1.0+s2, -r2, &tmod, &targ
                );
                check_gsl(status);
                glnmod -= tmod.val;
                sarg -= targ.val;

                status = gsl_sf_lngamma_complex_e(
                    1.0+s1+s2, r3, &tmod, &targ
                );
                check_gsl(status);
                glnmod -= tmod.val;
                sarg -= targ.val;

                status = gsl_sf_lngamma_complex_e(
                    (1.0-k1+k2+k3-tn)/2.0, -(r1+r2+r3)/2.0, &tmod, &targ
                );
                check_gsl(status);
                glnmod -= tmod.val;
                sarg -= targ.val;

                // add global phase from outside
                sarg += ephase;
                smod *= exp(glnmod);

                ncompl += smod * ((cos(sarg) + I * sin(sarg)) 
                        * complex_negpow(ts1+ts2-tk1+tk2));
                
            }
        }

        sum += nfact * ncompl;

    } 

    // real prefactor
    double pref = sqrt(
          gsl_sf_fact((unsigned int)(tj1 - tk1)/2)
        * gsl_sf_fact((unsigned int)(tj2 + tk2)/2)
        / (
            gsl_sf_fact((unsigned int)(tj1 + tk1)/2)
          * gsl_sf_fact((unsigned int)(tj2 - tk2)/2)
          * DIM(tj3)
        ));

    double complex result;
    result = pref * sum * complex_negpow(-tk1-tk2 + tj1-tj2+tj3);

    return result;

}

double B4_integrand(double r, void* params) {

    dspin* tjs = (dspin*)params;

    dspin tj1 = tjs[0];
    dspin tj2 = tjs[1];
    dspin tj3 = tjs[2];
    dspin tj4 = tjs[3];
    dspin tl1 = tjs[4];
    dspin tl2 = tjs[5];
    dspin tl3 = tjs[6];
    dspin tl4 = tjs[7];
    dspin ti  = tjs[8];
    dspin tk  = tjs[9];

    spin j1 = ((double)(tj1)) / 2.0;
    spin j2 = ((double)(tj2)) / 2.0;
    spin j3 = ((double)(tj3)) / 2.0;
    spin j4 = ((double)(tj4)) / 2.0;

    dspin TJ = tj1 + tj2 + tj3 + tj4;
    dspin TL = tl1 + tl2 + tl3 + tl4;

    spin J = TJ / 2;
    spin L = TL / 2;

    double sumh = 0.0;
    double result = 0.0;
    double complex ks, k1, k2, k3, k4, eph;
    double ksr = 0.0;

    // external phase factor
    eph = complex_negpow((dspin)(J-L)) * complex_negpow(ti-tk);

    int tm = min(ti, tk);
    dspin th;
    spin h;
    for (th = -tm; th <= tm; th += 2) {

        check_triangle_continue(tj1, tj2, ti);
        check_triangle_continue(tl1, tl2, tk);
        check_triangle_continue(tj3, tj4, ti);
        check_triangle_continue(tl3, tl4, tk);

        h = ((double)th)/2.0;

        sumh = (r*r + h*h);

        // after figuring out... 64 bits precision is way too low
        // for the Kerimov formula approach. The next trick
        // with conditional +/- computations works in pushing
        // double precision beyond spins ~10 but it's difficult
        // to trust it since instabilities are still present...

        // if (th < 0) { // || r > fmax(((double)tm)/4.0, 1.0)) {

        //     // k1 = conj(kappa_semi_simple(r, th,
        //     //         tj1, tj2, ti));
        //     // k2 = conj(kappa_semi_simple(r, th,
        //     //         tj3, tj4, ti));
        //     k1 = conj(kappa(GAMMA*j1, GAMMA*j2, r, 
        //         tj1, tj2, th,
        //         tj1, tj2, ti));
        //     k2 = conj(kappa(GAMMA*j3, GAMMA*j4, r,
        //         tj3, tj4, th,
        //         tj3, tj4, ti));
        //     k3 = kappa(GAMMA*j1, GAMMA*j2, r,
        //             tj1, tj2, th,
        //             tl1, tl2, tk);
        //     k4 = kappa(GAMMA*j3, GAMMA*j4, r,
        //             tj3, tj4, th,
        //             tl3, tl4, tk);

        //     // TRICK: multiply in following order to
        //     // help cancellation
        //     ks = (k1 * k3) * (k2 * k4);

        //     // add external phase factor
        //     ks *= eph;

        //     // should be real
        //     ksr = creal(ks);

        //     ksr /= (cos(M_PI*(j1 + j2 + h)) + cosh(M_PI*(GAMMA*j1 + GAMMA*j2 + r)));
        //     ksr /= (cos(M_PI*(j3 + j4 + h)) + cosh(M_PI*(GAMMA*j3 + GAMMA*j4 + r)));

        // } else {

        //     // k1 = conj(kappa_semi_simple(-r, -th,
        //     //         tj1, tj2, ti));
        //     // k2 = conj(kappa_semi_simple(-r, -th,
        //     //         tj3, tj4, ti));
        //     k1 = conj(kappa(GAMMA*j1, GAMMA*j2, -r, 
        //         tj1, tj2, -th,
        //         tj1, tj2, ti));
        //     k2 = conj(kappa(GAMMA*j3, GAMMA*j4, -r,
        //         tj3, tj4, -th,
        //         tj3, tj4, ti));
        //     k3 = kappa(GAMMA*j1, GAMMA*j2, -r,
        //             tj1, tj2, -th,
        //             tl1, tl2, tk);
        //     k4 = kappa(GAMMA*j3, GAMMA*j4, -r,
        //             tj3, tj4, -th,
        //             tl3, tl4, tk);

        //     // TRICK: multiply in following order to
        //     // help cancellation
        //     ks = (k1 * k3) * (k2 * k4);
            
        //     // add external phase factor
        //     ks *= eph;

        //     // should be real
        //     ksr = creal(ks);

        //     ksr /= (cos(M_PI*(j1 + j2 - h)) + cosh(M_PI*(GAMMA*j1 + GAMMA*j2 - r)));
        //     ksr /= (cos(M_PI*(j3 + j4 - h)) + cosh(M_PI*(GAMMA*j3 + GAMMA*j4 - r)));


        // }

        // k1 = conj(kappa(GAMMA*j1, GAMMA*j2, r, 
        //         tj1, tj2, th,
        //         tj1, tj2, ti));
        // k2 = conj(kappa(GAMMA*j3, GAMMA*j4, r,
        //         tj3, tj4, th,
        //         tj3, tj4, ti));
        // k3 = kappa(GAMMA*j1, GAMMA*j2, r,
        //         tj1, tj2, th,
        //         tl1, tl2, tk);
        // k4 = kappa(GAMMA*j3, GAMMA*j4, r,
        //         tj3, tj4, th,
        //         tl3, tl4, tk);

        k1 = conj(kappa(GAMMA*j1, GAMMA*j2, r, 
                tj1, tj2, th,
                tj1, tj2, ti));
        k2 = conj(kappa(GAMMA*j3, GAMMA*j4, r,
                tj3, tj4, th,
                tj3, tj4, ti));
        k3 = kappa(GAMMA*j1, GAMMA*j2, r,
                tj1, tj2, th,
                tl1, tl2, tk);
        k4 = kappa(GAMMA*j3, GAMMA*j4, r,
                tj3, tj4, th,
                tl3, tl4, tk);

        // TRICK: multiply in following order to
        // help cancellation
        ks = (k1 * k3) * (k2 * k4);

        // add external phase factor
        ks *= eph;

        // should be real
        ksr = creal(ks);

        ksr /= (cos(M_PI*(j1 + j2 + h)) + cosh(M_PI*(GAMMA*j1 + GAMMA*j2 + r)));
        ksr /= (cos(M_PI*(j3 + j4 + h)) + cosh(M_PI*(GAMMA*j3 + GAMMA*j4 + r)));

        sumh *= ksr;
        result += sumh;

    }

    return result;

}

double B4(dspin tj1, dspin tj2, dspin tj3, dspin tj4,
          dspin tl1, dspin tl2, dspin tl3, dspin tl4,
          dspin ti, dspin tk) {

    // check for mutual integrity/semi-integrity
    // of spins (comes from jappa coefficients)
    check_integer_2sum_return0(tj1, tl1);
    check_integer_2sum_return0(tj2, tl2);
    check_integer_2sum_return0(tj3, tl3);
    check_integer_2sum_return0(tj4, tl4);

    // check for mutual integrity/semi-integrity
    // of spins - intertwiner pairs
    // (comes from 4jm symbols)
    check_integer_3sum_return0(tj1, tj2, ti);
    check_integer_3sum_return0(tj3, tj4, ti);
    check_integer_3sum_return0(tl1, tl2, tk);
    check_integer_3sum_return0(tl3, tl4, tk);

    // test for trinagular inequalities
    check_triangle_return0(tj1, tj2, ti);
    check_triangle_return0(tj3, tj4, ti);
    check_triangle_return0(tl1, tl2, tk);
    check_triangle_return0(tl3, tj4, tk);

    // test for known zero cases
    check_zero_boost(tj1, tj2, tj3, tj4, tl1, tl2, tl3, tl4, ti, tk);

    double result, factor;
    
    factor = M_PI * M_PI / 32.0
           * DIM(ti) * DIM(tk)
           * sqrt(DIM(tj1) * DIM(tl1))
           * sqrt(DIM(tj2) * DIM(tl2))
           * sqrt(DIM(tj3) * DIM(tl3))
           * sqrt(DIM(tj4) * DIM(tl4));

    dspin params[10] = {tj1, tj2, tj3, tj4, tl1, tl2, tl3, tl4, ti, tk};

    gsl_function F;
    F.function = &B4_integrand;
    F.params = params;

    // perform integration
    double integral = boost_integration(&F);

    result = factor * integral;
    return result;

}

// TODO: comparing with the table from Speziale
//       I get all positive values -> check
double B3(dspin tj1, dspin tj2, dspin tj3,
          dspin tl1, dspin tl2, dspin tl3) {

    double result, factor;
    double complex ks;

    // check for J, L integers
    assert((tj1+tj2+tj3) % 2 == 0);
    assert((tl1+tl2+tl3) % 2 == 0);

    spin j1 = ((double)tj1)/2.0;
    spin j2 = ((double)tj2)/2.0;
    spin j3 = ((double)tj3)/2.0;

    dspin TJ = tj1 + tj2 + tj3;
    dspin TL = tl1 + tl2 + tl3;

    spin J = TJ / 2;
    spin L = TL / 2;

    double P = ((double)TJ)/2.0*GAMMA;
    double K = ((double)TJ)/2.0;

    factor = M_PI / 16.0
            * sqrt(DIM(tj1) * DIM(tl1))
            * sqrt(DIM(tj2) * DIM(tl2))
            * (DIM(tj3) * DIM(tl3));

    double complex f1, f2, f3; 
    
    f1 = complex_negpow((dspin)(J-L));

    f2 = conj(kappa(GAMMA*j1, GAMMA*j2, GAMMA*j3,
                tj1, tj2, tj3,
                tj1, tj2, tj3));
    f3 = kappa(GAMMA*j1, GAMMA*j2, GAMMA*j3,
                tj1, tj2, tj3,
                tl1, tl2, tl3);

    ks = f1 * (f2 * f3);

    // ks should be real, truncate the imaginary part
    result = factor
           * creal(ks) 
           / (cosh(M_PI*P) + cos(M_PI*K));

    return result;

}

#endif // #if USE_ARBITRARY_PRECISION == 0
