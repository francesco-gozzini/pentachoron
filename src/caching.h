#ifndef __PENTACHORON_CACHING_H__
#define __PENTACHORON_CACHING_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Hash table implementation with capped linked list at each
// table entry.
////////////////////////////////////////////////////////////////////////

#if USE_CACHING == 1

// Tries to retrieve a B4 coefficient from hash table cache.
// If not found it computes it and stores the values in the cache.
double get_B4_from_cache(
          dspin tj1, dspin tj2, dspin tj3, dspin tj4,
          dspin tl1, dspin tl2, dspin tl3, dspin tl4,
          dspin ti, dspin tk
        );

#endif 

			
/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_CACHING_H__*/