#ifndef __PENTACHORON_PLOT_H__
#define __PENTACHORON_PLOT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"
#include <gsl/gsl_integration.h>

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Functions for printing points of functions that are used
// in the program. Points are printed in format suitable for plotting
// in MATLAB.
////////////////////////////////////////////////////////////////////////

// Starts collecting points in an integrand functions. Program
// exits in the end if exit is set to true.
void dbg_start_plot(size_t npoints, bool exit);

// Collects a point from an integrand function.
void dbg_collect_point(double x, double y);

// Stops collecting points from an integrand function. Then
// prints the points and eventually terminates program.
void dbg_stop_plot();

// Prints the given function evaluated at the given points.
void plot(const gsl_function* f, double* X, size_t npoints);

// Print points in chosen format.
void print_points(double* X, double* Y, size_t npoints);
			
/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_PLOT_H__*/