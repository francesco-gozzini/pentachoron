#ifndef __PENTACHORON_UTILS_H__
#define __PENTACHORON_UTILS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <complex.h>
#include <time.h>
#include <stdio.h>
#include <assert.h>

#include "common.h"
#include "error.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Simple numerical utilities.
////////////////////////////////////////////////////////////////////////

// Macros for computing execution time of an instruction.
#define START_TIMING(t) {if ( (t = clock()) == -1) {error("Error calling clock");}}
#define STOP_TIMING(t) {t = (clock() - t);}
#define PRINT_TIMING(t) {printf("File %s, line %d - time measured: %gs.\n", \
        __FILE__, __LINE__, ((double) t / CLOCKS_PER_SEC));}
#define GET_TIMING(t) ((double) t / CLOCKS_PER_SEC)

// Checks for triangular inequalities.
#define check_triangle_return0(tj1, tj2, tj3) \
	if (!(abs(tj1-tj2) <= tj3 && tj3 <= (tj1 + tj2))) { \
		return 0; \
	}

#define check_triangle_continue(tj1, tj2, tj3) \
	if (!(abs(tj1-tj2) <= tj3 && tj3 <= (tj1 + tj2))) { \
		continue; \
	}

#define triangle(tj1, tj2, tj3) \
	(abs(tj1 - tj2) <= tj3 && tj3 <= (tj1 + tj2))


// Checks for integrity.
#define check_integer_2sum_return0(tl1, tl2) \
    { if ((tl1+tl2) % 2 != 0) {return 0;} }

#define check_integer_3sum_return0(tl1, tl2, tl3) \
    { if ((tl1+tl2+tl3) % 2 != 0) {return 0;} }

#define check_integer_2sum_continue(tl1, tl2) \
    { if ((tl1+tl2) % 2 != 0) {continue;} }

#define check_integer_3sum_continue(tl1, tl2, tl3) \
    { if ((tl1+tl2+tl3) % 2 != 0) {continue;} }

#define comp_sum(inp, sum, c, y, t) \
	{ y = inp - c; t = sum + y; c = (t - sum) - y; sum = t; }


// Computes (-1)^j for integer j.
// For semi-integer j it returns the real part.
static inline int real_negpow(dspin tj) {

	// ensure integer argument
    assert((tj % 2) == 0);

	int j = tj / 2;

	if (j % 2 == 0) {
	    return 1;
	} 
	return -1;

}

// Computes (-1)^j for semi-integer j.
static inline double complex complex_negpow(dspin tj) {

	int k = tj % 2; // i factor
	int j = tj / 2;

	if (k == 1) {
		if (j % 2 == 0) {
	    	return I;
		} 
		return -I;
	}

	if (k == -1) {
		if (j % 2 == 0) {
	    	return -I;
		} 
		return I;
	}

	if (j % 2 == 0) {
	    return 1;
	} 
	return -1;

}

// Returns the maximum of two integers.
static inline int max(int n1, int n2) {

	if (n1 > n2) {
		return n1;
	}
	return n2;

}

// Returns the minimum of two integers.
static inline int min(int n1, int n2) {

	if (n1 < n2) {
		return n1;
	}
	return n2;
	
}

// Returns sqrt(m! / n!).
double sqrt_frac_fact(int m, int n);

/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_UTILS_H__*/
