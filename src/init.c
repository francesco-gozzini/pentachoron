#include <gsl/gsl_errno.h>
#include <mpfr.h>

#include "common.h"
#include "pentachoron.h"
#include "integration.h"
#include "njm.h"
#include "cgamma.h"

bool initialized = false;

double GAMMA;

void pntc_init(struct pntc_setup* s) {

    // disable GSL default handler for errors
    gsl_set_error_handler_off();

    // set gamma
    GAMMA = s->gamma;

    // init wigxjpf
    init_wigxjpf();

    // init integration
    init_integration(
        s->ip->gsl_epsrel, 
        s->ip->upper_bound,
        s->ip->X,
        s->ip->totpoints
    );

#if USE_ARBITRARY_PRECISION == 1

    init_complex_gamma();
    mpfr_set_default_prec(PRECISION);

#endif

    initialized = true;

}

void pntc_free() {

    clear_integration();
    clear_wigxjpf();

#if USE_ARBITRARY_PRECISION == 1

    clear_complex_gamma();

#endif

    initialized = false;

}

bool lib_initialized() {
    return initialized;
}