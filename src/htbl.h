#ifndef __PENTACHORON_HTBL_H__
#define __PENTACHORON_HTBL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Hash table implementation with capped linked list at each
// table entry.
////////////////////////////////////////////////////////////////////////


// Node for the linked list at each table entry.
// Dimension: 10 integers + 2 double + pointer ~ 70 byte.
// TODO: consider declare dspin as byte to lower memory usage
typedef struct b4_node
{
    dspin tj1;
    dspin tj2;
    dspin tj3;
    dspin tj4;
    dspin tl1;
    dspin tl2;
    dspin tl3;
    dspin tl4;
    dspin ti;
    dspin tk;
    double value;
    double dtime;
    struct b4_node* next;
}
b4_node;

// Inserts a coefficient into the hash table.
// Returns true if inserted, false otherwise.
bool b4_insert(uint32_t key,
               dspin tj1, dspin tj2, dspin tj3, dspin tj4,
               dspin tl1, dspin tl2, dspin tl3, dspin tl4,
               dspin ti, dspin tk, double value, double dtime); 

// Computes an integer hash for the given spins list.
uint32_t b4_hash(dspin tj1, dspin tj2, dspin tj3, dspin tj4,
                 dspin tl1, dspin tl2, dspin tl3, dspin tl4,
                 dspin ti, dspin tk);

// Searches for a stored coefficient into the hash table given
// the list of spins. Returns true if found, false otherwise.
bool b4_get_value(uint32_t key,
                  dspin tj1, dspin tj2, dspin tj3, dspin tj4,
                  dspin tl1, dspin tl2, dspin tl3, dspin tl4,
                  dspin ti, dspin tk, double* value);

/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_HTBL_H__*/