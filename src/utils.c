#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "utils.h"


double sqrt_frac_fact(int m, int n) {

	int _min = min(m,n);
	int _max = max(m,n);
	int M;

	double res = 1.0;
	for (M = _min + 1; M <= _max; M++) {

		if (_min == m) {
			res /= sqrt((double)M);
		} else {
			res *= sqrt((double)M);
		}

	}

	return res;

}
