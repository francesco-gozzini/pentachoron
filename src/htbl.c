#include <stdlib.h>

#include "htbl.h"
#include "common.h"
#include "error.h"

// number of hash indices
#define HASH_CELLS 100000

// max number of values stored for each index
// the maximum amount of memory taken will be
// = (node dimension) * HASH_CELLS * MAX_PER_CELL bytes
//
// TODO: check that list does not saturate
#define MAX_PER_CELL 200

b4_node* b4_first[HASH_CELLS] = {NULL};

bool b4_insert(uint32_t key,
               dspin tj1, dspin tj2, dspin tj3, dspin tj4,
               dspin tl1, dspin tl2, dspin tl3, dspin tl4,
               dspin ti, dspin tk, double value, double dtime) {

    // check for empty list
    if (b4_first[key] != NULL) {

        // check for insertion at tail
        b4_node* prevptr = b4_first[key];
        int traversed = 0;

        // the last node points to the first
        // at second run substitute the first node found which takes less
        // time to be computed

        while (true) {

            traversed++;

            if (traversed > 2*MAX_PER_CELL) {
                return false;
            }

            if (traversed > MAX_PER_CELL) {
                
                // 2nd run; check time
                if (prevptr->dtime < dtime) {

                    // substitute
                    prevptr->tj1 = tj1;
                    prevptr->tj2 = tj2;
                    prevptr->tj3 = tj3;
                    prevptr->tj4 = tj4;
                    prevptr->tl1 = tl1;
                    prevptr->tl2 = tl2;
                    prevptr->tl3 = tl3;
                    prevptr->tl4 = tl4;
                    prevptr->ti = ti;
                    prevptr->tk = tk;
                    prevptr->value = value;
                    prevptr->dtime = dtime;

                    return true;

                }

            }

            // insert at tail
            if (prevptr->next == NULL) {

                // try to instantiate node
                b4_node* newptr = (b4_node*) malloc(sizeof(b4_node));
                if (newptr == NULL) {
                    error("malloc failed");
                }

                // make a new pointer
                newptr->tj1 = tj1;
                newptr->tj2 = tj2;
                newptr->tj3 = tj3;
                newptr->tj4 = tj4;
                newptr->tl1 = tl1;
                newptr->tl2 = tl2;
                newptr->tl3 = tl3;
                newptr->tl4 = tl4;
                newptr->ti = ti;
                newptr->tk = tk;
                newptr->value = value;
                newptr->dtime = dtime;

                // check if circular insertion or new insertion
                if (traversed == MAX_PER_CELL) {
                    newptr->next = b4_first[key];
                } else {
                    newptr->next = NULL;
                }
            
                prevptr->next = newptr;

                return true;

            }

            // update pointer
            prevptr = prevptr->next;

        }

    } else {

        // first allocation

        // try to instantiate node
        b4_node* newptr = (b4_node*) malloc(sizeof(b4_node));
        if (newptr == NULL)
        {
            error("malloc failed");
        }

        // make a new pointer
        newptr->tj1 = tj1;
        newptr->tj2 = tj2;
        newptr->tj3 = tj3;
        newptr->tj4 = tj4;
        newptr->tl1 = tl1;
        newptr->tl2 = tl2;
        newptr->tl3 = tl3;
        newptr->tl4 = tl4;
        newptr->ti = ti;
        newptr->tk = tk;
        newptr->value = value;
        newptr->dtime = dtime;
        newptr->next = NULL;

        b4_first[key] = newptr;

        return true;

    }

    return false;

}

uint32_t b4_hash(dspin tj1, dspin tj2, dspin tj3, dspin tj4,
                 dspin tl1, dspin tl2, dspin tl3, dspin tl4,
                 dspin ti, dspin tk) {

    uint32_t hash = 0xA1E1305F;

    hash ^= ((uint32_t)DIM(tk) << 12);
    hash ^= ((uint32_t)DIM(ti) << 24);
    
    hash ^= ((uint32_t)DIM(tj1) << 5);
    hash ^= ((uint32_t)DIM(tj2) << 16);
    hash ^= ((uint32_t)DIM(tj3) << 27);
    hash ^= ((uint32_t)DIM(tj4) << 8);
    hash ^= ((uint32_t)DIM(tl1) << 19);
    hash ^= ((uint32_t)DIM(tl2) << 30);
    hash ^= ((uint32_t)DIM(tl3) << 11);
    hash ^= ((uint32_t)DIM(tl4) << 22);

    return hash % HASH_CELLS;

}

bool b4_get_value(uint32_t key,
                  dspin tj1, dspin tj2, dspin tj3, dspin tj4,
                  dspin tl1, dspin tl2, dspin tl3, dspin tl4,
                  dspin ti, dspin tk, double* value) {

    if (b4_first[key] == NULL) {

        return false;

    } else {

        // check for insertion at tail
        b4_node* prevptr = b4_first[key];
        int traversed = 0;

        while (true) {

            if (prevptr == NULL) {
                return false;
            }

            traversed++;

            if (traversed > MAX_PER_CELL) {
                return false;
            }

            if (
                prevptr->tj1 == tj1 &&
                prevptr->tj2 == tj2 &&
                prevptr->tj3 == tj3 &&
                prevptr->tj4 == tj4 &&
                prevptr->tl1 == tl1 &&
                prevptr->tl2 == tl2 &&
                prevptr->tl3 == tl3 &&
                prevptr->tl4 == tl4 &&
                prevptr->ti == ti &&
                prevptr->tk == tk
            ) {
                *value = prevptr->value;
                return true;
            }

            // update pointer
            prevptr = prevptr->next;
        
        }

    }

    return false;

}