#include <math.h>
#include <stdio.h>
#include <mpfr.h>

#include "pentachoron.h"
#include "njm.h"
#include "error.h"
#include "common.h"
#include "utils.h"
#include "caching.h"
#include "boosts.h"

// "barriers" for estimating non-negligible contributions
// when building sub-amplitudes for summing them up 
#define BARRIER_1 1e-3
#define BARRIER_2 1e-5
#define BARRIER_3 1e-7
#define BARRIER_4 1e-9
#define BARRIER_5 1e-12

// wrap function to allow for caching
static double compute_B4(
          dspin tj1, dspin tj2, dspin tj3, dspin tj4,
          dspin tl1, dspin tl2, dspin tl3, dspin tl4,
          dspin ti, dspin tk) {

    /////////////////////////////////////////////////
    // report the same null checks performed in B4
    // computations for performance: waste of memory
    // to store a bunch of zeros in cache
    /////////////////////////////////////////////////

    check_integer_2sum_return0(tj1, tl1);
    check_integer_2sum_return0(tj2, tl2);
    check_integer_2sum_return0(tj3, tl3);
    check_integer_2sum_return0(tj4, tl4);

    check_integer_3sum_return0(tj1, tj2, ti);
    check_integer_3sum_return0(tj3, tj4, ti);
    check_integer_3sum_return0(tl1, tl2, tk);
    check_integer_3sum_return0(tl3, tl4, tk);

    check_triangle_return0(tj1, tj2, ti);
    check_triangle_return0(tj3, tj4, ti);
    check_triangle_return0(tl1, tl2, tk);
    check_triangle_return0(tl3, tj4, tk);

    check_zero_boost(tj1, tj2, tj3, tj4, tl1, tl2, tl3, tl4, ti, tk);

    ////////////////////////////////////////////////

#if USE_CACHING

    // try to get from table
    return get_B4_from_cache(tj1, tj2, tj3, tj4,
                             tl1, tl2, tl3, tl4,
                             ti, tk);

#else

    return B4(tj1, tj2, tj3, tj4,
              tl1, tl2, tl3, tl4,
              ti, tk);

#endif
}

#if USE_ARBITRARY_PRECISION

double pntc_amplitude(struct pntc_amplitude_params* p, struct pntc_boundary_state* b) {

    if (!lib_initialized()) {
        error("library not initialized");
    }

    double res;

    mpfr_t ampl;
    mpfr_init2(ampl, PRECISION);
    mpfr_set_zero(ampl, 0);

    dspin tj1, tj2, tj3, tj4, tj5,
          tj6, tj7, tj8, tj9, tj10;

    tj1 = b->tjs[0];
    tj2 = b->tjs[1];
    tj3 = b->tjs[2];
    tj4 = b->tjs[3];
    tj5 = b->tjs[4];
    tj6 = b->tjs[5];
    tj7 = b->tjs[6];
    tj8 = b->tjs[7];
    tj9 = b->tjs[8];
    tj10 = b->tjs[9];

    dspin tl1, tl2, tl3, tl4, tl5,
          tl6, tl7, tl8, tl9, tl10;

    // 4 ls are fixed on the boundary
    tl5 = tj5;
    tl6 = tj6;
    tl9 = tj9;
    tl10 = tj10;

    dspin ti1, ti2, ti3, ti4, ti5;

    ti1 = b->tis[0];
    ti2 = b->tis[1];
    ti3 = b->tis[2];
    ti4 = b->tis[3];
    ti5 = b->tis[4];

    dspin dtl = p->delta_tl;

    if ((dtl % 2) != 0) {
        error("integer Delta-l required");
    }

    double wig15j;

    mpfr_t Ae1, Ae2, Ae3, Ae5, dampl;
    mpfr_init2(Ae1, PRECISION);
    mpfr_init2(Ae2, PRECISION);
    mpfr_init2(Ae3, PRECISION);
    mpfr_init2(Ae5, PRECISION);
    mpfr_init2(dampl, PRECISION);

    int lstep = 2;
    int kstep = 2;

#if USE_REDUCIBLE_COUPLING

    dspin mink11, mink12, mink1, maxk11, maxk12, maxk1, tk1;
    dspin mink2, maxk2, tk2;
    dspin mink3, maxk3, tk3;
    dspin mink5, maxk5, tk5;

    /////////////////////////////////////////////////////////////////
    // LOOP OVER VIRTUAL SPINS
    /////////////////////////////////////////////////////////////////

    for (tl1 = tj1; tl1 <= tj1 + dtl; tl1 += lstep) {
    for (tl2 = tj2; tl2 <= tj2 + dtl; tl2 += lstep) {
    for (tl3 = tj3; tl3 <= tj3 + dtl; tl3 += lstep) {
    for (tl4 = tj4; tl4 <= tj4 + dtl; tl4 += lstep) {
    for (tl7 = tj7; tl7 <= tj7 + dtl; tl7 += lstep) {
    for (tl8 = tj8; tl8 <= tj8 + dtl; tl8 += lstep) {

        /////////////////////////////////////////////////////////////////
        // LOOP OVER VIRTUAL INTERTWINERS
        /////////////////////////////////////////////////////////////////

        if (p->verbose) {
            printf("\n##########################################################\n");
            printf("tis: %d %d %d %d %d | tls: %d %d %d %d %d %d %d %d %d %d\n", 
                    ti1, ti2, ti3, ti4, ti5,
                    tl1, tl2, tl3, tl4, tl5, tl6, tl7, tl8, tl9, tl10);
            printf("##########################################################\n");
        }

        // second virtual intertwiner
        // l10 l7 l1 l2
        mink2 = (dspin) max(abs(tl10-tl7), abs(tl1-tl2));
        maxk2 = (dspin) min(tl10+tl7, tl1+tl2);
        for (tk2 = mink2; tk2 <= maxk2; tk2 += kstep) {
            check_integer_3sum_continue(tl10, tl7, tk2);
            check_integer_3sum_continue(tl1, tl2, tk2);

            mpfr_set_d(Ae2, compute_B4(
                tj10, tj7, tj1, tj2,
                tl10, tl7, tl1, tl2,
                ti2, tk2
            ), MPFR_RNDN);

            if (p->verbose) {
                printf("2: B4(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d) = %g | ", 
                       tj10, tj7, tj1, tj2, tl10, tl7, tl1, tl2, ti2, tk2, mpfr_get_d(Ae2, MPFR_RNDN)
                );
            }

            if (mpfr_zero_p(Ae2)) {
                continue;
            }

        // third virtual intertwiner 
        // l9 l8 l3 l1
        mink3 = (dspin) max(abs(tl9-tl8), abs(tl3-tl1));
        maxk3 = (dspin) min(tl9+tl8, tl3+tl1);
        for (tk3 = mink3; tk3 <= maxk3; tk3 += kstep) {
            check_integer_3sum_continue(tl9, tl8, tk3);
            check_integer_3sum_continue(tl3, tl1, tk3);

            mpfr_set_d(Ae3, compute_B4(
                tj9, tj8, tj3, tj1,
                tl9, tl8, tl3, tl1,
                ti3, tk3
            ), MPFR_RNDN);

            if (p->verbose) { 
                printf("3: B4(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d) = %g | ", 
                    tj9, tj8, tj3, tj1, tl9, tl8, tl3, tl1, ti3, tk3, mpfr_get_d(Ae3, MPFR_RNDN)
                );
            }

            if (mpfr_zero_p(Ae3)) {
                continue;
            }

        // fifth virtual intertwiner
        // l4 l6 l7 l8
        mink5 = (dspin) max(abs(tl4-tl6), abs(tl7-tl8));
        maxk5 = (dspin) min(tl4+tl6, tl7+tl8);
        for (tk5 = mink5; tk5 <= maxk5; tk5 += kstep) {
            check_integer_3sum_continue(tl4, tl6, tk5);
            check_integer_3sum_continue(tl7, tl8, tk5);

            mpfr_set_d(Ae5, compute_B4(
                tj4, tj6, tj7, tj8,
                tl4, tl6, tl7, tl8,
                ti5, tk5
            ), MPFR_RNDN);

            if (p->verbose) {
                printf("5: B4(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d) = %g | ", 
                    tj4, tj6, tj7, tj8, tl4, tl6, tl7, tl8, ti5, tk5, mpfr_get_d(Ae5, MPFR_RNDN)
                ); 
            }

            if (mpfr_zero_p(Ae5)) {
                continue;
            }

        // first virtual intertwiner
        // l2 l3 l5 l4
        mink11 = (dspin) max(abs(tl2-tl3), abs(tl5-tl4));
        mink12 = (dspin) max(abs(tk2-tk3), abs(tk5-ti4));
        mink1 = max(mink11, mink12);
        maxk11 = (dspin) min(tl2+tl3, tl5+tl4);
        maxk12 = (dspin) min(tk2+tk3, tk5+ti4);
        maxk1 = min(maxk11, maxk12);
        for (tk1 = mink1; tk1 <= maxk1; tk1 += kstep) {
            check_integer_3sum_continue(tl2, tl3, tk1);
            check_integer_3sum_continue(tl5, tl4, tk1);

            mpfr_set_d(Ae1, compute_B4(
                tj2, tj3, tj5, tj4,
                tl2, tl3, tl5, tl4,
                ti1, tk1
            ), MPFR_RNDN);

            if (p->verbose) {
                printf("1: B4(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d) = %g | ", 
                    tj2, tj3, tj5, tj4, tl2, tl3, tl5, tl4, ti1, tk1, mpfr_get_d(Ae1, MPFR_RNDN)
                ); 
            }

            if (mpfr_zero_p(Ae1)) {
                continue;
            }


            // 15j for reducible coupling
            // (the spins are rearranged inside the function)
            wig15j = wigner_15j_reducible(
                tk1, tk2, tk3, ti4, tk5,
                tl1, tl2, tl3, tl4, tl5,
                tl6, tl7, tl8, tl9, tl10
            );

            if (p->verbose) { printf("15j = %g | ", wig15j); }

            if (wig15j == 0) {
                continue;
            }

            mpfr_set_d(dampl, (double)(DIM(tk1)*DIM(tk2)*DIM(tk3)*DIM(tk5)), MPFR_RNDN);
            mpfr_mul_d(dampl, dampl, wig15j, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae1, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae2, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae3, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae5, MPFR_RNDN);

            if (p->verbose) { printf("dampl = %g\n", mpfr_get_d(dampl, MPFR_RNDN)); }

            mpfr_add(ampl, ampl, dampl, MPFR_RNDN);

        } // tk1
        } // tk2
        } // tk3
        } // tk5
        
    } // tl1
    } // tl2
    } // tl3
    } // tl4
    } // tl7
    } // tl8

#else

    dspin mink1, maxk1, tk1;
    dspin mink2, maxk2, tk2;
    dspin mink3, maxk3, tk3;
    dspin mink5, maxk5, tk5;

    /////////////////////////////////////////////////////////////////
    // LOOP OVER VIRTUAL SPINS
    /////////////////////////////////////////////////////////////////

    for (tl1 = tj1; tl1 <= tj1 + dtl; tl1 += lstep) {
    for (tl2 = tj2; tl2 <= tj2 + dtl; tl2 += lstep) {
    for (tl3 = tj3; tl3 <= tj3 + dtl; tl3 += lstep) {
    for (tl4 = tj4; tl4 <= tj4 + dtl; tl4 += lstep) {
    for (tl7 = tj7; tl7 <= tj7 + dtl; tl7 += lstep) {
    for (tl8 = tj8; tl8 <= tj8 + dtl; tl8 += lstep) {

        /////////////////////////////////////////////////////////////////
        // LOOP OVER VIRTUAL INTERTWINERS
        /////////////////////////////////////////////////////////////////

        if (p->verbose) {
            printf("\n##########################################################\n");
            printf("tis: %d %d %d %d %d | tls: %d %d %d %d %d %d %d %d %d %d\n", 
                    ti1, ti2, ti3, ti4, ti5,
                    tl1, tl2, tl3, tl4, tl5, tl6, tl7, tl8, tl9, tl10);
            printf("##########################################################\n");
        }

        // first virtual intertwiner
        // l2 l3 l5 l4
        mink1 = (dspin) max(abs(tl2-tl3), abs(tl5-tl4));
        maxk1 = (dspin) min(tl2+tl3, tl5+tl4);
        for (tk1 = mink1; tk1 <= maxk1; tk1 += kstep) {
            check_integer_3sum_continue(tl2, tl3, tk1);
            check_integer_3sum_continue(tl5, tl4, tk1);

            mpfr_set_d(Ae1, compute_B4(
                tj2, tj3, tj5, tj4,
                tl2, tl3, tl5, tl4,
                ti1, tk1
            ), MPFR_RNDN);

            if (p->verbose) { printf("Ae1 = %g | ", mpfr_get_d(Ae1, MPFR_RNDN)); }

            if (mpfr_zero_p(Ae1)) {
                continue;
            }

        // second virtual intertwiner
        // l1 l10 l7 l2
        mink2 = (dspin) max(abs(tl1-tl10), abs(tl7-tl2));
        maxk2 = (dspin) min(tl1+tl10, tl7+tl2);
        for (tk2 = mink2; tk2 <= maxk2; tk2 += kstep) {
            check_integer_3sum_continue(tl1, tl10, tk2);
            check_integer_3sum_continue(tl7, tl2, tk2);

            mpfr_set_d(Ae2, compute_B4(
                tj1, tj10, tj7, tj2,
                tl1, tl10, tl7, tl2,
                ti2, tk2
            ), MPFR_RNDN);

            if (p->verbose) { printf("Ae2 = %g | ", mpfr_get_d(Ae2, MPFR_RNDN)); }

            if (mpfr_zero_p(Ae2)) {
                continue;
            }

        // third virtual intertwiner 
        // l9 l8 l3 l1
        mink3 = (dspin) max(abs(tl9-tl8), abs(tl3-tl1));
        maxk3 = (dspin) min(tl9+tl8, tl3+tl1);
        for (tk3 = mink3; tk3 <= maxk3; tk3 += kstep) {
            check_integer_3sum_continue(tl9, tl8, tk3);
            check_integer_3sum_continue(tl3, tl1, tk3);

            mpfr_set_d(Ae3, compute_B4(
                tj9, tj8, tj3, tj1,
                tl9, tl8, tl3, tl1,
                ti3, tk3
            ), MPFR_RNDN);

            if (p->verbose) { printf("Ae3 = %g | ", mpfr_get_d(Ae3, MPFR_RNDN)); }

            if (mpfr_zero_p(Ae3)) {
                continue;
            }

        // fifth virtual intertwiner
        // l4 l7 l8 l6
        mink5 = (dspin) max(abs(tl4-tl7), abs(tl8-tl6));
        maxk5 = (dspin) min(tl4+tl7, tl8+tl6);
        for (tk5 = mink5; tk5 <= maxk5; tk5 += kstep) {
            check_integer_3sum_continue(tl4, tl7, tk5);
            check_integer_3sum_continue(tl8, tl6, tk5);

            mpfr_set_d(Ae5, compute_B4(
                tj4, tj7, tj8, tj6,
                tl4, tl7, tl8, tl6,
                ti5, tk5
            ), MPFR_RNDN);

            if (p->verbose) { printf("Ae5 = %g | ", mpfr_get_d(Ae5, MPFR_RNDN)); }

            if (mpfr_zero_p(Ae5)) {
                continue;
            }

            // 15j for irreducible coupling
            // (the spins are rearranged here to match previous convention)
            wig15j = wigner_15j(
                tk1, tl5, ti4, tl10, tk2,  // j
                tl4, tl6, tl9, tl1,  tl2,  // l
                tl7, tk5, tl8, tk3,  tl3   // k
            );

            if (p->verbose) { printf("15j = %g | ", wig15j); }

            if (wig15j == 0) {
                continue;
            }

            mpfr_set_d(dampl, (double)(DIM(tk1)*DIM(tk2)*DIM(tk3)*DIM(tk5)), MPFR_RNDN);
            mpfr_mul_d(dampl, dampl, wig15j, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae1, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae2, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae3, MPFR_RNDN);
            mpfr_mul(dampl, dampl, Ae5, MPFR_RNDN);

            if (p->verbose) { printf("dampl = %g\n", mpfr_get_d(dampl, MPFR_RNDN)); }

            mpfr_add(ampl, ampl, dampl, MPFR_RNDN);

        } // tk1
        } // tk2
        } // tk3
        } // tk5
        
    } // tl1
    } // tl2
    } // tl3
    } // tl4
    } // tl7
    } // tl8

#endif

    res = DIM(ti1) * DIM(ti2) * DIM(ti3)
        * DIM(ti4) * DIM(ti5) * mpfr_get_d(ampl, MPFR_RNDN);
    
    mpfr_clears(ampl, dampl, Ae1, Ae2, Ae3, Ae5, (mpfr_ptr) 0);

    return res;

    // TODO: there are still cases where the amplitude
    //       should be zero because of cancellations by the symmetries
    //       of the 15js. Es the amplitude with all j == 3, i == 1.
    //       Check these cases.

}

#else

double pntc_amplitude(struct pntc_amplitude_params* p, struct pntc_boundary_state* b) {

    if (!lib_initialized()) {
        error("library not initialized");
    }

    double ampl, c, y, t;
    ampl = 0.0;
    c = 0.0;

    dspin tj1, tj2, tj3, tj4, tj5,
          tj6, tj7, tj8, tj9, tj10;

    tj1 = b->tjs[0];
    tj2 = b->tjs[1];
    tj3 = b->tjs[2];
    tj4 = b->tjs[3];
    tj5 = b->tjs[4];
    tj6 = b->tjs[5];
    tj7 = b->tjs[6];
    tj8 = b->tjs[7];
    tj9 = b->tjs[8];
    tj10 = b->tjs[9];

    dspin tl1, tl2, tl3, tl4, tl5,
          tl6, tl7, tl8, tl9, tl10;

    // 4 ls are fixed on the boundary
    tl5 = tj5;
    tl6 = tj6;
    tl9 = tj9;
    tl10 = tj10;

    dspin ti1, ti2, ti3, ti4, ti5;

    ti1 = b->tis[0];
    ti2 = b->tis[1];
    ti3 = b->tis[2];
    ti4 = b->tis[3];
    ti5 = b->tis[4];

    dspin dtl = p->delta_tl;

    if ((dtl % 2) != 0) {
        error("integer Delta-l required");
    }

    double wig15j;
    double Ae1, Ae2, Ae3, Ae5; //, Ae4;
    double dampl;

    int lstep = 2;
    int kstep = 2;

#if USE_REDUCIBLE_COUPLING == 1

    dspin mink11, mink12, mink1, maxk11, maxk12, maxk1, tk1;
    dspin mink2, maxk2, tk2;
    dspin mink3, maxk3, tk3;
    dspin mink5, maxk5, tk5;

    /////////////////////////////////////////////////////////////////
    // LOOP OVER VIRTUAL SPINS
    /////////////////////////////////////////////////////////////////

    for (tl1 = tj1; tl1 <= tj1 + dtl; tl1 += lstep) {
    for (tl2 = tj2; tl2 <= tj2 + dtl; tl2 += lstep) {
    for (tl3 = tj3; tl3 <= tj3 + dtl; tl3 += lstep) {
    for (tl4 = tj4; tl4 <= tj4 + dtl; tl4 += lstep) {
    for (tl7 = tj7; tl7 <= tj7 + dtl; tl7 += lstep) {
    for (tl8 = tj8; tl8 <= tj8 + dtl; tl8 += lstep) {

        /////////////////////////////////////////////////////////////////
        // LOOP OVER VIRTUAL INTERTWINERS
        /////////////////////////////////////////////////////////////////

        if (p->verbose) {
            printf("\n##########################################################\n");
            printf("tis: %d %d %d %d %d | tls: %d %d %d %d %d %d %d %d %d %d\n", 
                    ti1, ti2, ti3, ti4, ti5,
                    tl1, tl2, tl3, tl4, tl5, tl6, tl7, tl8, tl9, tl10);
            printf("##########################################################\n");
        }

        // second virtual intertwiner
        // l10 l7 l1 l2
        mink2 = (dspin) max(abs(tl10-tl7), abs(tl1-tl2));
        maxk2 = (dspin) min(tl10+tl7, tl1+tl2);
        for (tk2 = mink2; tk2 <= maxk2; tk2 += kstep) {
            check_integer_3sum_continue(tl10, tl7, tk2);
            check_integer_3sum_continue(tl1, tl2, tk2);

            Ae2 = compute_B4(
                tj10, tj7, tj1, tj2,
                tl10, tl7, tl1, tl2,
                ti2, tk2
            );

            if (p->verbose) { printf("Ae2 = %g | ", Ae2); }

            if (Ae2 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae2/ampl) < BARRIER_1) {
                if (p->verbose) { printf("CUTTING at BARRIER 1 | "); }
                continue;
            }

        // third virtual intertwiner 
        // l9 l8 l3 l1
        mink3 = (dspin) max(abs(tl9-tl8), abs(tl3-tl1));
        maxk3 = (dspin) min(tl9+tl8, tl3+tl1);
        for (tk3 = mink3; tk3 <= maxk3; tk3 += kstep) {
            check_integer_3sum_continue(tl9, tl8, tk3);
            check_integer_3sum_continue(tl3, tl1, tk3);

            Ae3 = compute_B4(
                tj9, tj8, tj3, tj1,
                tl9, tl8, tl3, tl1,
                ti3, tk3
            );

            if (p->verbose) { printf("Ae3 = %g | ", Ae3); }

            if (Ae3 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae2*Ae3/ampl) < BARRIER_2) {
                if (p->verbose) { printf("CUTTING at BARRIER 2 | "); }
                continue;
            }

        // fifth virtual intertwiner
        // l4 l6 l7 l8
        mink5 = (dspin) max(abs(tl4-tl6), abs(tl7-tl8));
        maxk5 = (dspin) min(tl4+tl6, tl7+tl8);
        for (tk5 = mink5; tk5 <= maxk5; tk5 += kstep) {
            check_integer_3sum_continue(tl4, tl6, tk5);
            check_integer_3sum_continue(tl7, tl8, tk5);

            Ae5 = compute_B4(
                tj4, tj6, tj7, tj8,
                tl4, tl6, tl7, tl8,
                ti5, tk5
            );

            if (p->verbose) { printf("Ae5 = %g | ", Ae5); }

            if (Ae5 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae2*Ae3*Ae5/ampl) < BARRIER_3) {
                if (p->verbose) { printf("CUTTING at BARRIER 3 | "); }
                continue;
            }

        // first virtual intertwiner
        // l2 l3 l5 l4
        mink11 = (dspin) max(abs(tl2-tl3), abs(tl5-tl4));
        mink12 = (dspin) max(abs(tk2-tk3), abs(tk5-ti4));
        mink1 = max(mink11, mink12);
        maxk11 = (dspin) min(tl2+tl3, tl5+tl4);
        maxk12 = (dspin) min(tk2+tk3, tk5+ti4);
        maxk1 = min(maxk11, maxk12);
        for (tk1 = mink1; tk1 <= maxk1; tk1 += kstep) {
            check_integer_3sum_continue(tl2, tl3, tk1);
            check_integer_3sum_continue(tl5, tl4, tk1);

            Ae1 = compute_B4(
                tj2, tj3, tj5, tj4,
                tl2, tl3, tl5, tl4,
                ti1, tk1
            );

            if (p->verbose) { printf("Ae1 = %g | ", Ae1); }

            if (Ae1 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae2*Ae3*Ae5*Ae1/ampl) < BARRIER_4) {
                if (p->verbose) { printf("CUTTING at BARRIER 4 | "); }
                continue;
            }

            // 15j for reducible coupling
            // (the spins are rearranged inside the function)
            wig15j = wigner_15j_reducible(
                tk1, tk2, tk3, ti4, tk5,
                tl1, tl2, tl3, tl4, tl5,
                tl6, tl7, tl8, tl9, tl10
            );

            if (p->verbose) { printf("15j = %g | ", wig15j); }

            if (wig15j == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae2*Ae3*Ae5*Ae1*wig15j/ampl) < BARRIER_5) {
                if (p->verbose) { printf("CUTTING at BARRIER 5 | "); }
                continue;
            }

            // sum up
            dampl = DIM(tk1) * Ae1
                  * DIM(tk2) * Ae2
                  * DIM(tk3) * Ae3
                  * DIM(tk5) * Ae5
                  * wig15j;

            if (p->verbose) { printf("dampl = %g\n", dampl); }

            // compensated summation
            comp_sum(dampl, ampl, c, y, t);

        } // tk1
        } // tk2
        } // tk3
        } // tk5
        
    } // tl1
    } // tl2
    } // tl3
    } // tl4
    } // tl7
    } // tl8

#else

    dspin mink1, maxk1, tk1;
    dspin mink2, maxk2, tk2;
    dspin mink3, maxk3, tk3;
    dspin mink5, maxk5, tk5;

    /////////////////////////////////////////////////////////////////
    // LOOP OVER VIRTUAL SPINS
    /////////////////////////////////////////////////////////////////

    for (tl1 = tj1; tl1 <= tj1 + dtl; tl1 += lstep) {
    for (tl2 = tj2; tl2 <= tj2 + dtl; tl2 += lstep) {
    for (tl3 = tj3; tl3 <= tj3 + dtl; tl3 += lstep) {
    for (tl4 = tj4; tl4 <= tj4 + dtl; tl4 += lstep) {
    for (tl7 = tj7; tl7 <= tj7 + dtl; tl7 += lstep) {
    for (tl8 = tj8; tl8 <= tj8 + dtl; tl8 += lstep) {

        /////////////////////////////////////////////////////////////////
        // LOOP OVER VIRTUAL INTERTWINERS
        /////////////////////////////////////////////////////////////////

        if (p->verbose) {
            printf("\n##########################################################\n");
            printf("tis: %d %d %d %d %d | tls: %d %d %d %d %d %d %d %d %d %d\n", 
                    ti1, ti2, ti3, ti4, ti5,
                    tl1, tl2, tl3, tl4, tl5, tl6, tl7, tl8, tl9, tl10);
            printf("##########################################################\n");
        }

        // first virtual intertwiner
        // l2 l3 l5 l4
        mink1 = (dspin) max(abs(tl2-tl3), abs(tl5-tl4));
        maxk1 = (dspin) min(tl2+tl3, tl5+tl4);
        for (tk1 = mink1; tk1 <= maxk1; tk1 += kstep) {
            check_integer_3sum_continue(tl2, tl3, tk1);
            check_integer_3sum_continue(tl5, tl4, tk1);

            Ae1 = compute_B4(
                tj2, tj3, tj5, tj4,
                tl2, tl3, tl5, tl4,
                ti1, tk1
            );

            if (p->verbose) { printf("Ae1 = %g | ", Ae1); }

            if (Ae1 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae1/ampl) < BARRIER_1) {
                if (p->verbose) { printf("CUTTING at BARRIER 1 | "); }
                continue;
            }

        // second virtual intertwiner
        // l1 l10 l7 l2
        mink2 = (dspin) max(abs(tl1-tl10), abs(tl7-tl2));
        maxk2 = (dspin) min(tl1+tl10, tl7+tl2);
        for (tk2 = mink2; tk2 <= maxk2; tk2 += kstep) {
            check_integer_3sum_continue(tl1, tl10, tk2);
            check_integer_3sum_continue(tl7, tl2, tk2);

            Ae2 = compute_B4(
                tj1, tj10, tj7, tj2,
                tl1, tl10, tl7, tl2,
                ti2, tk2
            );

            if (p->verbose) { printf("Ae2 = %g | ", Ae2); }

            if (Ae2 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae1*Ae2/ampl) < BARRIER_2) {
                if (p->verbose) { printf("CUTTING at BARRIER 2 | "); }
                continue;
            }

        // third virtual intertwiner 
        // l9 l8 l3 l1
        mink3 = (dspin) max(abs(tl9-tl8), abs(tl3-tl1));
        maxk3 = (dspin) min(tl9+tl8, tl3+tl1);
        for (tk3 = mink3; tk3 <= maxk3; tk3 += kstep) {
            check_integer_3sum_continue(tl9, tl8, tk3);
            check_integer_3sum_continue(tl3, tl1, tk3);

            Ae3 = compute_B4(
                tj9, tj8, tj3, tj1,
                tl9, tl8, tl3, tl1,
                ti3, tk3
            );

            if (p->verbose) { printf("Ae3 = %g | ", Ae3); }

            if (Ae3 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae1*Ae2*Ae3/ampl) < BARRIER_3) {
                if (p->verbose) { printf("CUTTING at BARRIER 3 | "); }
                continue;
            }

        // fifth virtual intertwiner
        // l4 l7 l8 l6
        mink5 = (dspin) max(abs(tl4-tl7), abs(tl8-tl6));
        maxk5 = (dspin) min(tl4+tl7, tl8+tl6);
        for (tk5 = mink5; tk5 <= maxk5; tk5 += kstep) {
            check_integer_3sum_continue(tl4, tl7, tk5);
            check_integer_3sum_continue(tl8, tl6, tk5);

            Ae5 = compute_B4(
                tj4, tj7, tj8, tj6,
                tl4, tl7, tl8, tl6,
                ti5, tk5
            );

            if (p->verbose) { printf("Ae5 = %g | ", Ae5); }

            if (Ae5 == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae1*Ae2*Ae3*Ae5/ampl) < BARRIER_4) {
                if (p->verbose) { printf("CUTTING at BARRIER 4 | "); }
                continue;
            }

            // 15j for irreducible coupling
            // (the spins are rearranged here to match previous convention)
            wig15j = wigner_15j(
                tk1, tl5, ti4, tl10, tk2,  // j
                tl4, tl6, tl9, tl1,  tl2,  // l
                tl7, tk5, tl8, tk3,  tl3   // k
            );

            if (p->verbose) { printf("15j = %g | ", wig15j); }

            if (wig15j == 0) {
                continue;
            }

            if (ampl != 0 && fabs(Ae1*Ae2*Ae3*Ae5*wig15j/ampl) < BARRIER_5) {
                if (p->verbose) { printf("CUTTING at BARRIER 5 | "); }
                continue;
            }

            // sum up
            dampl = DIM(tk1) * Ae1
                  * DIM(tk2) * Ae2
                  * DIM(tk3) * Ae3
                  * DIM(tk5) * Ae5
                  * wig15j;

            if (p->verbose) { printf("dampl = %g\n", dampl); }

            // compensated summation
            comp_sum(dampl, ampl, c, y, t);

        } // tk1
        } // tk2
        } // tk3
        } // tk5
        
    } // tl1
    } // tl2
    } // tl3
    } // tl4
    } // tl7
    } // tl8

#endif

    return DIM(ti1) * DIM(ti2) * DIM(ti3)
         * DIM(ti4) * DIM(ti5) * ampl;

    // TODO: there are still cases where the amplitude
    //       should be zero because of cancellations by the symmetries
    //       of the 15js. Es the amplitude with all j == 3, i == 1.
    //       Check these cases.

}

#endif

double pntc_amplitude_BF(struct pntc_amplitude_params* p, struct pntc_boundary_state* b) {

    if (!lib_initialized()) {
        error("library not initialized");
    }

    dspin tj1, tj2, tj3, tj4, tj5,
          tj6, tj7, tj8, tj9, tj10;

    tj1 = b->tjs[0];
    tj2 = b->tjs[1];
    tj3 = b->tjs[2];
    tj4 = b->tjs[3];
    tj5 = b->tjs[4];
    tj6 = b->tjs[5];
    tj7 = b->tjs[6];
    tj8 = b->tjs[7];
    tj9 = b->tjs[8];
    tj10 = b->tjs[9];

    dspin ti1, ti2, ti3, ti4, ti5;

    ti1 = b->tis[0];
    ti2 = b->tis[1];
    ti3 = b->tis[2];
    ti4 = b->tis[3];
    ti5 = b->tis[4];

    double wig15j;

#if USE_REDUCIBLE_COUPLING

            // 15j for reducible coupling
            // (the spins are rearranged inside the function)
            wig15j = wigner_15j_reducible(
                ti1, ti2, ti3, ti4, ti5,
                tj1, tj2, tj3, tj4, tj5,
                tj6, tj7, tj8, tj9, tj10
            );

            if (p->verbose) { printf("15j = %g | ", wig15j); }

#else

            // 15j for irreducible coupling
            // (the spins are rearranged here to match previous convention)
            wig15j = wigner_15j(
                ti1, tj5, ti4, tj10, ti2,  // j
                tj4, tj6, tj9, tj1,  tj2,  // l
                tj7, ti5, tj8, ti3,  tj3   // k
            );

            if (p->verbose) { printf("15j = %g | ", wig15j); }

#endif

    return DIM(ti1) * DIM(ti2) * DIM(ti3)
         * DIM(ti4) * DIM(ti5) * wig15j;

}
