#ifndef __PENTACHORON_NJM_H__
#define __PENTACHORON_NJM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "utils.h"

/**********************************************************************/

////////////////////////////////////////////////////////////////////////
// Functions for computing the needed wigner j/jm symbols.
////////////////////////////////////////////////////////////////////////

// Always call this before using the functions for wigner symbols.
void init_wigxjpf();

// Always call this when finished to work with wigner symbols.
void clear_wigxjpf();

// Per-thread WIGXJPF init functions.
void init_wigxjpf_thread();
void clear_wigxjpf_thread();

// Returns true if wigxjpf has been correctly initialized.
bool wig_initialized();

// Computes the wigner 15j symbol using a particular
// couplig to split it into 6j*6j*9j.
double wigner_15j_reducible(
		    dspin ti1, dspin ti2, dspin ti3, dspin ti4, dspin ti5,
		    dspin tj1, dspin tj2, dspin tj3, dspin tj4, dspin tj5,
		    dspin tj6, dspin tj7, dspin tj8, dspin tj9, dspin tj10);

// Computes the wigner 15j symbol of first type. 
double wigner_15j(
		    dspin ti1, dspin ti2, dspin ti3, dspin ti4, dspin ti5,
		    dspin tj1, dspin tj2, dspin tj3, dspin tj4, dspin tj5,
		    dspin tj6, dspin tj7, dspin tj8, dspin tj9, dspin tj10);	

// Computes the wigner 3jm symbol.
double wigner_3jm(
			dspin tj1, dspin tj2, dspin tj3,
			dspin tm1, dspin tm2, dspin tm3);
			
/**********************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__PENTACHORON_NJM_H__*/
