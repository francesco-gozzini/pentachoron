#include <stdio.h>
#include <mpfr.h>
#include <omp.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_integration.h>

#include "integration.h"
#include "error.h"
#include "utils.h"
#include "config.h"
#include "njm.h"

#define GSL_NPOINTS 100

double integration_upper_bound;
double* integration_simp_X;
size_t integration_simp_totpoints;
gsl_integration_workspace* gsl_wrk;
unsigned int integration_gsl_points;
double integration_gsl_epsrel;

void init_integration(double gsl_epsrel, double ub, double* X, size_t totpoints) {

    integration_upper_bound = ub;

    // init GSL integration
    integration_gsl_epsrel = gsl_epsrel;
    gsl_wrk = gsl_integration_workspace_alloc(GSL_NPOINTS);

    // init my integration
    integration_simp_X = X;
    integration_simp_totpoints = totpoints;

}

void clear_integration() {
    gsl_integration_workspace_free(gsl_wrk);
}

double boost_integration(const gsl_function* f) {

    #if USE_ARBITRARY_PRECISION && PARALLELIZE

    return integrate_simple_parallel(f, integration_simp_X, integration_simp_totpoints);

    #else

    return integrate_simple(f, integration_simp_X, integration_simp_totpoints);

    #endif

}

double integrate_simple(const gsl_function* f, double* X, size_t totpoints) {

    size_t i;
    double dx;
    double sum, c, v, t;
    double yi, yip;

    sum = 0.0;
    c = 0.0;
    yi = f->function(X[0], f->params);
    for (i = 0; i < totpoints - 1; i++) {

        dx = X[i+1] - X[i];
        yip = f->function(X[i+1], f->params);

        // compensated summation
        comp_sum((yi + yip) * dx, sum, c, v, t);

        yi = yip;

    }

    return sum * 0.5;

}

#if USE_ARBITRARY_PRECISION && PARALLELIZE

double integrate_simple_parallel(const gsl_function* f, double* X, size_t totpoints) {

    double ret;

    mpfr_t sum;
    mpfr_init(sum);
    mpfr_set_zero(sum, 1);

    #pragma omp parallel
    {

        double dx;
        double yi, yip;

        init_wigxjpf_thread();

        mpfr_t dint;
        mpfr_init(dint);
        
        #pragma omp for
        for (size_t i = 0; i < totpoints - 1; i++) {

            dx = X[i+1] - X[i];
            yi = f->function(X[i], f->params);
            yip = f->function(X[i+1], f->params);

            mpfr_set_d(dint, yi, MPFR_RNDN);
            mpfr_add_d(dint, dint, yip, MPFR_RNDN);
            mpfr_mul_d(dint, dint, dx, MPFR_RNDN);

            #pragma omp critical
            mpfr_add(sum, sum, dint, MPFR_RNDN);

        }

        mpfr_clear(dint);
        clear_wigxjpf_thread();

    }

    ret = 0.5 * mpfr_get_d(sum, MPFR_RNDN);
    mpfr_clear(sum);

    return ret;

}

#endif

void uniform_grid(double* X, double a, double b, size_t npoints) {

    double h = fabs(b-a)/(double)(npoints - 1 );
    size_t i;
    for (i = 0; i < npoints; i++) {
        X[i] = a + (double)i * h;
    }

}

void piecewise_grid(double* X, double* xs, size_t* npoints, size_t nxs) {

    size_t i, j, done;
    double xi, hi;
    size_t ni;
    
    done = 0;
    for (i = 0; i < nxs - 1; i++) {

        xi = xs[i];
        ni = npoints[i];
        hi = fabs(xs[i+1]-xs[i])/(double)ni;
        for (j = done; j < done + ni; j++) {
            X[j] = xi + (double)(j-done) * hi;
        }
        done += ni;

    }

}

