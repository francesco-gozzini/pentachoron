QUIET ?= @

CC = gcc
AR = ar

## development
CFLAGS = -O0 -fPIC -fopenmp -Wall -Wextra -Wconversion -Wcast-qual -Wshadow -Wno-format -g -I../wigxjpf-1.9/inc/ -Iinc/ -Isrc/

## release
#CFLAGS = -O2 -fopenmp -fPIC -I../wigxjpf-1.9/inc/ -Iinc/ -Isrc/

## profiling
#LDFLAGS = -pg -no-pie -L../wigxjpf-1.9/lib/ -fopenmp

## development
LDFLAGS = -L../wigxjpf-1.9/lib/ -fopenmp

LDLIBS = -lwigxjpf -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lm

SRCDIR = src
OBJDIR = obj
INCDIR = inc
LIBDIR = lib
BINDIR = bin
TESTDIR = test

default: lib
all: default

INCS = src/utils.h src/njm.h src/boosts.h inc/pentachoron.h src/common.h \
	   src/error.h src/plot.h src/caching.h src/htbl.h src/integration.h \
	   src/cgamma.h src/config.h

_OBJS = utils.o njm.o boosts.o ampl.o init.o plot.o caching.o htbl.o \
	    integration.o cgamma.o boosts_ap.o
		
OBJS = $(patsubst %,$(OBJDIR)/%,$(_OBJS))

# library object files
$(OBJDIR)/%.o: $(SRCDIR)/%.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $<

# tests binaries object files
$(OBJDIR)/%.o: $(TESTDIR)/%.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $< -I$(SRCDIR)/ -I$(INCDIR)/

# correlation binaries object files
$(OBJDIR)/amplitudes.o: corr/amplitudes.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $< -I$(SRCDIR)/ -I$(INCDIR)/

# correlation binaries object files
$(OBJDIR)/amplext.o: corrext/amplext.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) -fopenmp $(CFLAGS) -c -o $@ $< -I$(SRCDIR)/ -I$(INCDIR)/ -I../sl2cfoam-dev/inc/

$(LIBDIR)/libpntc.a: $(OBJS)
	@echo "   AR    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(AR) $(ARFLAGS) $@ $(OBJS)

$(BINDIR)/test: $(OBJS) $(OBJDIR)/test.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) -o $@ $(OBJDIR)/test.o $(OBJS) $(LDFLAGS) $(LDLIBS)

$(BINDIR)/hashtest: $(OBJS) $(OBJDIR)/hashtest.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) -o $@ $(OBJDIR)/hashtest.o $(OBJS) $(LDFLAGS) $(LDLIBS)

$(BINDIR)/libtest: $(LIBDIR)/libpntc.a $(OBJDIR)/libtest.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) -o $@ $(OBJDIR)/libtest.o $(LDFLAGS) -L$(LIBDIR)/ -lpntc $(LDLIBS)

$(BINDIR)/amplitudes: $(LIBDIR)/libpntc.a $(OBJDIR)/amplitudes.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) -o $@ $(OBJDIR)/amplitudes.o -pg $(LDFLAGS) -L$(LIBDIR)/ -lpntc $(LDLIBS)

$(BINDIR)/amplext: $(OBJDIR)/amplext.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) -o $@ $(OBJDIR)/amplext.o $(LDFLAGS) -L../sl2cfoam-dev/lib/ -lsl2cfoam $(LDLIBS)

.PHONY: default all clean

lib: $(LIBDIR)/libpntc.a

test: $(BINDIR)/test

hashtest: $(BINDIR)/hashtest

libtest: $(BINDIR)/libtest

amplitudes: $(BINDIR)/amplitudes

amplext: $(BINDIR)/amplext

clean:
	rm -rf $(OBJDIR)
	rm -rf $(LIBDIR)
	rm -rf $(BINDIR)
